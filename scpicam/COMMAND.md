SCPICAM commands and functions
===========

1. Global commands
-----------------------------

General parameter commands
-----------
- SYSTem:ERRor? - Get error code
- SYSTem:ERRor:COUNt? - Get error count
- SYSTem:VERSion? - Get scpicam version
- SYSTem:DATE <yyyy>, <mm>, <dd> - Set current date on system
- SYSTem:DATE? - Get current date on system
- SYSTem:TIME <hh>, <mm>, <ss> - Set current time on system
- SYSTem:TIME? - Get current time on system
- STATus:PRESet - Restart raspberry pi system
- PROGram:STATe *(RUN | PAUSe | STOP | CONTinue)* - Manage program state.
- **IDN��* - This order can read information about power supply. The parameter it returns contains 4 segments divided by comma. 
	For example��calHelp, SCPICAM, 0001 ,V1.00

Memory commands
-----------
- MEMory:INITialize - Load CAMera:MEMory:LOAD init and further more
- MEMory:DELete *FILENAME* - Global command for managing all configuration on system
- MEMory:LOAD *FILENAME* - Global command for managing all configuration on system
- MEMory:STORe *FILENAME* - Global command for managing all configuration on system

Communicate commands
-----------
The communicate subsystem is used to set the controls and the parameters associated with serial system communication.

- :SYSTem:COMMunicate:SERial:RESet - Reset the RS-232 buffer. This will discard any unprocessed SCPI input received via the RS-232 port. There are no initial values associated with this command.
- :SYSTem:COMMunicate:SERial:BAUD <number> - Set the baud rate for the rear panel RS-232 interface (labeled AUXILIARY INTERFACE). The baud rate must be set to 19200 if an optional remote interface box is connected. The choices for the variable are 300, 1200, 2400, 4800, 9600, 19200, 38400. This is a persistent state set to 19200 at default.
- :SYSTem:COMMunicate:SERial:BAUD? - Get the current baud rate for RS-232 interface
- :SYSTem:COMMunicate:SERial:CONTrol:RTS ON|OFF|IBFull|RFR - Control the state of the RS-232 RTS line. The choices are ON, OFF, RFR, and IBFull.
	ON - for connecting the optional remote interface box. The instrument ignores the state of the CTS line.
	OFF - for a three-wire remote connection. With this setting, the instrument ignores the state of the CTS line. This setting is not compatible with the optional remote interface box.
	IBFull or RFR - turn on handshaking. Do not use these settings without a properly configured remote connection that uses hardwire handshaking. With IBFull or RFR, when the receive buffer of the instrument is near overflow, the RTS line is turned off. With RFR or IBFull, the instrument monitors the state of the CTS line, and if it goes false discontinues transmitting over RS-232. This setting is not compatible with the optional remote interface box. This is a persistent state set to ON at default.
- :SYSTem:COMMunicate:SERial:CONTrol:RTS? - Get the state of the RS-232 RTS line
- :SYSTem:COMMunicate:SERial:ECHO ON|OFF|1|0 - Control the state of the RS-232 echo. The choices are On (1) of Off (0). This is a persistent state set to Off at default.
- :SYSTem:COMMunicate:SERial:ECHO? - Get the state of the RS-232 echo
- :SYSTem:COMMunicate:SERial:TOUT <val> - Set the value for the RS-232 serial port timeout. If further input is not received within the assigned timeout period while a SCPI command is being processed, then the command is aborted and the input buffer is cleared. This is a persistent state set to 60 seconds at default.
- :SYSTem:COMMunicate:SERial:TOUT? - Get the value for the RS-232 serial port timeout
- :SYSTem:COMMunicate:SERial:RECeive:PACE XON|NONE - Set XON/XOFF handshaking when the instrument is receiving data. The choices are XON and None. This is a persistent state, set to None at default.
- :SYSTem:COMMunicate:SERial:RECeive:PACE? - Get XON/XOFF handshaking when the instrument is receiving data
- :SYSTem:COMMunicate:SERial:TRANsmit:PACE XON|NONE - Set XON/XOFF handshaking when the instrument is transmitting data. The choices are XON and None.
- :SYSTem:COMMunicate:SERial:TRANsmit:PACE? - Get XON/XOFF handshaking when the instrument is transmitting data.
- :SYSTem:COMMunicate:TCPip:CONTrol - Set a unique dynamic port number for TCP socket connections. This command returns zero for any connection except TCP sockets.
- :SYSTem:COMMunicate:TCPip:CONTrol? - Get current dynamic port number for TCP socket connections.
- :SYST:COMM:LAN:ADDRess - Set ip address on system.
- :SYST:COMM:LAN:HOSTname - Set hostname on system.
- :SYST:COMM:LAN:SUBnetMask - Set ip subnet mask on system
- :SYST:COMM:LAN:GATEway - Set default gateway on system
- :SYST:COMM:LAN:NAMESERVER1 - Set nameserver1 on system
- :SYST:COMM:LAN:NAMESERVER2 - Set nameserver2
- :SYST:COMM:LAN:RESet - Reset LAN interface. If you set LAN related commands, please execute this command.

2. Camera commands
-----------------------------

Camera common parameter commands
-----------
- CAMera:CONFigure:OPTions:WIDth *SIZE* - Set image width
- CAMera:CONFigure:OPTions:WIDth? - Get image width
- CAMera:CONFigure:OPTions:HEIght *SIZE* - Set image height
- CAMera:CONFigure:OPTions:HEIght? - Get image height
- CAMera:CONFigure:OPTions:QUAlity *<0~100>* - Set jpeg quality. Quality 100 is almost completely uncompressed. 75 is a good allround value.
- CAMera:CONFigure:OPTions:QUAlity? - Get jpeg quality
- CAMera:CONFigure:OPTions:RAW - Add raw Bayer data to jpeg metadata. This option inserts the raw Bayer data from the camera in to the JPEG metadata.
- CAMera:CONFigure:OPTions:OUTput *FILENAME* - Specify the output filename. If not specified, no file is saved.
- CAMera:CONFigure:OPTions:OUTput? - Get output filename
- CAMera:CONFigure:OPTions:VERbose - Output verbose information during run. Outputs debugging/information messages during the program run.
- CAMera:CONFigure:OPTions:TIMeout *TIME* - Time before capture and shut down. The program will run for this length of time, then take the capture (if output is specified). If not specified, this is set to 5 seconds. (seconds unit)
- CAMera:CONFigure:OPTions:TIMeout? - Get current timeout
- CAMera:CONFigure:OPTions:THUmb *<X>,<Y>,<0~100>* - Set thumbnail parameters(x, y, quality). Allows specification of the thumbnail image inserted in to the JPEG file. If not specified, defaults are a size of 64x48 at quality 35.
- CAMera:CONFigure:OPTions:THUmb? - Get current thumbnail parameters
- CAMera:CONFigure:OPTions:DEMo *TIME* - Run a demo mode. This options cycles through range of camera options, and no capture is done. The demo will end at the end of the timeout period, irrespective of whether all the options have been cycled. The time between cycles should be specified as a millisecond value.
- CAMera:CONFigure:OPTions:DEMo? - Get current demo mode
- CAMera:CONFigure:OPTions:ENCoding *(jpg | gif | png | bmp)* - Encoding to use as output file. Valid options are jpg, bmp, gif and png. Note that unaccelerated image types (gif, png, bmp) will take much longer to save than jpg, which is hardware accelerated. Also note that the filename suffix is completely ignored when encoding a file.
- CAMera:CONFigure:OPTions:ENCoding? - Get current encoding type
- CAMera:CONFigure:OPTions:EXIf *<key=value>* - EXIF tag to apply to captures
- CAMera:CONFigure:OPTions:TIMElapse *TIME* - Timelapse mode. The specific value is the time between shots in milliseconds. Note you should specify %04d at the point in the filename where you want a frame count number to appear. (seconds unit)
CAMera:CONFigure:OPTions:TIMElapse?

Camera preview parameter commands
-----------
- CAMera:CONFigure:VIEw:PREview *<X>,<Y>,<W>,<H>* - Preview window settings. Allows the user to define the size and location on the screen that the preview window will be placed. Note this will be superimposed over the top of any other windows/graphics.
- CAMera:CONFigure:VIEw:FULlscreen - Fullscreen preview mode. Forces the preview window to use the whole screen. Note that the aspect ratio of the incoming image will be retained, so there may be bars on some edges.
- CAMera:CONFigure:VIEw:NOPreview - Do not display a preview window. Disables the preview window completely. Note that even though the preview is disabled, the camera will still be producing frames, so will be using power.

Camera image parameter commands
-----------
- CAMera:CONFigure:IMAge:SHArpness *<-100~100>* - Set image sharpness
- CAMera:CONFigure:IMAge:CONtrast *<-100~100>* - Set image contrast
- CAMera:CONFigure:IMAge:BRIghtness *<0~100>* - Set image brightness
- CAMera:CONFigure:IMAge:SATuration *<-100~100>* - Set image saturation
- CAMera:CONFigure:IMAge:ISO *<100~800>* - Set capture ISO
- CAMera:CONFigure:IMAge:EV *<-10~10>* - Set EV compensation
- CAMera:CONFigure:IMAge:EXPosure *EXP* - Set exposure mode.
	Possible options are:
	off
	auto        Use automatic exposure mode
	night       Select setting for night shooting
	nightpreview
	backlight   Select setting for back-lit subject
	spotlight
	sports      Select setting for sports (fast shutter etc.)
	snow        Select setting optimized for snowy scenery
	beach       Select setting optimized for beach
	verylong    Select setting for long exposures
	fixedfps    Constrain fps to a fixed value
	antishake   Antishake mode
	fireworks   Select setting optimized for fireworks
	Note that not all of these settings may be implemented, depending on camera tuning.
- CAMera:CONFigure:IMAge:AWB *AWB* - Set automatic white balance.
	off          Turn off white balance calculation
	auto         Automatic mode (default)
	sun          Sunny mode
	cloudshade   Cloudy mode
	tungsten     Tungsten lighting mode
	fluorescent  Fluorescent lighting mode
	incandescent Incandescent lighting mode
	flash        Flash mode
	horizon      Horizon mode
- CAMera:CONFigure:IMAge:IMXfx *IFX* - Set image effect.
	none         No effect
	negative     Produces a negative image
	solarise     Solarise the image
	whiteboard   Whiteboard effect
	blackboard   Blackboard effect
	sketch       Sketch-style effect
	denoise      Denoise the image
	emboss       Embossed effect
	oilpaint     Oil paint-style effect
	hatch        Cross-hatch sketch style
	gpen         Graphite sketch style
	pastel       Pastel effect
	watercolour  Watercolour effect
	film         Grainy film effect
	blur         Blur the image
	saturation   Colour-saturate the image
	colourswap   Not fully implemented
	washedout    Not fully implemented
	posterise    Not fully implemented
	colourpoint  Not fully implemented
	colourbalance Not fully implemented
	cartoon      Not fully implemented
- CAMera:CONFigure:IMAge:COLfx *<U>,<V>* - Set colour effect. The supplied U and V parameters (range 0 to 255) are applied to the U and Y channels of the image.
- CAMera:CONFigure:IMAge:METering *MODE* - Set metering mode. Specify the metering mode used for the preview and capture.
	average      Average the whole frame for metering
	spot Spot    metering
	backlit      Assume a backlit image
	matrix       Matrix metering
- CAMera:CONFigure:IMAge:ROTation *DEGREE* - Set image rotation. Sets the rotation of the image in viewfinder and resulting image. This can take any value from 0 upwards, but due to hardware constraints only 0, 90, 180 and 270-degree rotations are supported.
- CAMera:CONFigure:IMAge:SHArpness? - Get current sharpness
- CAMera:CONFigure:IMAge:CONtrast? - Get current contrast
- CAMera:CONFigure:IMAge:BRIghtness? - Get current brightness
- CAMera:CONFigure:IMAge:SATuration? - Get current saturation
- CAMera:CONFigure:IMAge:ISO? - Get current ISO
- CAMera:CONFigure:IMAge:EV? - Get current EV
- CAMera:CONFigure:IMAge:EXPosure? - Get current exposure
- CAMera:CONFigure:IMAge:AWB? - Get current awb
- CAMera:CONFigure:IMAge:IMXfx? - Get current image effect
- CAMera:CONFigure:IMAge:COLfx? - Get current UV parameters
- CAMera:CONFigure:IMAge:METering? - Get current metering mode
- CAMera:CONFigure:IMAge:ROTation? - Get current rotation
- CAMera:CONFigure:IMAge:VSTab - Turn on video stabilization. In video mode only, turn on video stabilization
- CAMera:CONFigure:IMAge:HFLip - Set horizontal flip. Flips the preview and saved image horizontally
- CAMera:CONFigure:IMAge:VFLip- Set vertical flip. Flips the preview and saved image vertically

Camera memory parameter commands
-----------
- CAMera:MEMory:STORe *FILENAME* - Save all configurations for camera to FILENAME.
- CAMera:MEMory:DELete *FILENAME* - Delete configuration file
- CAMera:MEMory:LOAD *FILENAME* - Load all configurations for camera from FILENAME

Camera runtime parameter commands
-----------
- CAMera:STATe *(RUN | STOP)* - Run/Stop camera with already configured commands


3. OCR commands
-----------------------------

OCR options commands
-----------
- OCR:CONFigure:OPTions:VERBose - Some messages regarding program execution are printed.
- OCR:CONFigure:OPTions:THReshold *THRESHOLD* - Set the luminance threshold used to distinguish black from white. THRESHOLD is interpreted as a percentage.
- OCR:CONFigure:OPTions:ABSThreshold - Use the THRESHOLD value without adjustment. Otherwise the THRESHOLD is adjusted to the luminance interval used in the image.
- OCR:CONFigure:OPTions:ITRThreshold - Use an iterative method (one-dimensional k-means clustering) to determine the threshold used to distinguish black from white. A THRESHOLD value given via -t THRESHOLD sets the starting value.
- OCR:CONFigure:OPTions:NUMPixels *NUMBER* - Set the number of foreground pixels that have to be found in a scanline to recognize a segment. Can be used to ignore some noise in the picture.
- OCR:CONFigure:OPTions:IGNPixels *NUMBER* - Set the number of foreground pixels that are ignored when deciding if a column consists only of background or foreground pixels. Can be used to ignore some noise in the picture.
- OCR:CONFigure:OPTions:DIGNumbers *NUMBER* - Specifies the number of digits shown in the image. If NUMBER is -1, the number of digits is detected automatically.
- OCR:CONFigure:OPTions:ONERatio *RATIO* - Set the height/width ratio threshold to recognize a digit as a one. RATIO takes integers only.
- OCR:CONFigure:OPTions:MINRatio *RATIO* - Set the width/height ratio to recognize a minus sign. RATIO takes integers only.
- OCR:CONFigure:OPTions:OUTImage *FILENAME* - Write the processed image to FILE. Normally no image is written to disk. If a standard extension is used it is interpreted as the image format to use.
- OCR:CONFigure:OPTions:OUTFormat FORMAT - Specify the image format. This format must be recognized by Imlib2, standard filename extensions are used.
- OCR:CONFigure:OPTions:PROcess - Process the given commands only, no segmentation or character recognition. Should be used together with --output-image=FILE.
- OCR:CONFigure:OPTions:DBGImage *FILENAME* - Write a debug image showing the results of thresholding, segmentation and character recognition. The image is written to testbild.png unless a filename FILE is given.
- OCR:CONFigure:OPTions:DBGOutput - Print information helpful for debugging.
- OCR:CONFigure:OPTions:FOREground *(black | white)* - Specify the foreground color (either black or white). This automatically sets the background color as well.
- OCR:CONFigure:OPTions:BACKground *(black | white)* - Specify the background color (either black or white). This automatically sets the foreground color as well.
- OCR:CONFigure:OPTions:PRNInfo - Prints image dimensions and range of used luminance values.
- OCR:CONFigure:OPTions:ADJGray - Use values T1 and T2 given to command gray_stretch as percentages instead of absolut luminance values.
- OCR:CONFigure:OPTions:LUMInance *KEYWORD* - Controls the kind of luminace computation. Using help as KEYWORD prints the list of keywords with a short description of the used formula. The default should work well. 
- OCR:CONFigure:OPTions:THReshold? - Get current threshold.
- OCR:CONFigure:OPTions:ABSThreshold? - Get current absolute threshold
- OCR:CONFigure:OPTions:ITERThreshold? - Get current iterative threshold
- OCR:CONFigure:OPTions:NUMPixels? - Get current number of foreground pixels
- OCR:CONFigure:OPTions:IGNPixels? - Get current number of foreground pixels that are ignored
- OCR:CONFigure:OPTions:DIGNumbers? - Get current number of digits shown in the image
- OCR:CONFigure:OPTions:ONERatio? - Get current height/width ratio threshold to recognize a digit as a one
- OCR:CONFigure:OPTions:MINRatio? - Get current width/height ratio to recognize a minus sign
- OCR:CONFigure:OPTions:OUTImage? - Get current output filename
- OCR:CONFigure:OPTions:OUTFormat? - Get current output format
- OCR:CONFigure:OPTions:FOREground? - Get current foreground color
- OCR:CONFigure:OPTions:BACKground? - Get current background color
- OCR:CONFigure:OPTions:ADJGray? - Get current gray_stretch state
- OCR:CONFigure:OPTions:LUMInance? - Get current luminance

    Luminance Keywords: rec601, rec709, linear, minimum, maximum, red, green, blue

OCR control commands
-----------
- OCR:CONFigure:COMmands:DILAtion - Filter image using dilation algorithm. Any pixel with at least one neighbor pixel set in the source image will be set in the filtered image.
- OCR:CONFigure:COMmands:EROSion - Filter image using erosion algorithm. Any pixel with every neighbor pixel set in the source image will be set in the filtered image.
- OCR:CONFigure:COMmands:CLOSing *N* - Filter image using closing algorithm, i.e. erosion and then dilation. If a number N>1 is specified, N times dilation and then N times erosion is executed.
- OCR:CONFigure:COMmands:OPENing *N* - Filter image using opening algorithm, i.e. dilation and then erosion. If a number N>1 is specified, N times dilation and then N times erosion is executed.
- OCR:CONFigure:COMmands:RMVIsolated - Remove any foreground pixels without neighboring foreground pixels.
- OCR:CONFigure:COMmands:GRAYscale - Transform image to gray values using luminance. The formula to compute luminance can be specified using option --luminance.
- OCR:CONFigure:COMmands:MONO - Convert the image to monochrome using thresholding. The threshold can be specified with option --threshold and is adjusted to the used luminance interval of the image unless option --absolute-threshold is specified.
- OCR:CONFigure:COMmands:INVert - Set every foreground pixel to background color and vice versa. It is faster to set the foreground color to white than to invert the image.
- OCR:CONFigure:COMmands:STRetch *T1,T2* - Transform image so that the luminance interval [T1,T2] is projected to [0,255] with any value below T1 set to 0 and any value above T2 set to 255.
- OCR:CONFigure:COMmands:DYNamic *W,H* - Convert the image to monochrome using dynamic thresholding a.k.a local adaptive thresholding. A window of width W and height H around the current pixel is used.
- OCR:CONFigure:COMmands:RGB - Convert the image to monochrome using simple thresholding for every color channel. If any of the red, green or blue values is below the threshold, the pixel is set to black. You should use --luminance=minimum and make_mono or dynamic_threshold instead.
- OCR:CONFigure:COMmands:RTHreshold - Convert the image to monochrome using simple thresholding. Only the red color channel is used. You should use --luminance=red and make_mono or dynamic_threshold instead. 
- OCR:CONFigure:COMmands:GTHreshold - Convert the image to monochrome using simple thresholding. Only the green color channel is used. You should use --luminance=green and make_mono or dynamic_threshold instead. 
- OCR:CONFigure:COMmands:BTHreshold - Convert the image to monochrome using simple thresholding. Only the blue color channel is used. You should use --luminance=blue and make_mono or dynamic_threshold instead. 
- OCR:CONFigure:COMmands:WHIte *WIDTH* - The border of the image is set to the foreground color. This border is one pixel wide unless a WIDTH>1 is specified.
- OCR:CONFigure:COMmands:SHEar *OFFSET* - Shear the image OFFSET pixels to the right. The OFFSET is used at the bottom. Image dimensions do not change, pixels in background color are used for pixels that are outside the image and shifted inside. Pixels shifted out of the image are dropped. Many seven segment displays use slightly skewed digits, this command can be used to compensate this. 
- OCR:CONFigure:COMmands:ROTate *THETA* - Rotate the image THETA degrees clockwise around the center of the image. Image dimensions do not change, pixels rotated out of the image area are dropped, pixels from outside the image rotated into the new image are set to the background color. 
- OCR:CONFigure:COMmands:MIRror *(horiz | vert)* - Mirror the image either horizontally (horiz) or vertically (vert). 
- OCR:CONFigure:COMmands:CROP *X,Y,W,H* - Use only the subpicture with upper left corner (X,Y), width W and height H.
- OCR:CONFigure:COMmands:SETPixels *MASK* - Set every pixel in the filtered image that has at least MASK neighbor pixels set in the source image.
- OCR:CONFigure:COMmands:KEEPixels *MASK* - Keep only those foreground pixels in the filtered image that have at least MASK neighbor pixels set in the source image (not counting the checked pixel itself).
- OCR:CONFigure:COMmands:CLOSing? - Get current closing state
- OCR:CONFigure:COMmands:OPENing? - Get current opening state
- OCR:CONFigure:COMmands:STRetch? - Get current stretch T1, T2
- OCR:CONFigure:COMmands:DYNamic? - Get current dynamic thresholding
- OCR:CONFigure:COMmands:WHIte? - Get current border of the image
- OCR:CONFigure:COMmands:SHEar? - Get current image OFFSET pixels to the right
- OCR:CONFigure:COMmands:ROTate? - Get current degrees clockwise around the center of the image
- OCR:CONFigure:COMmands:MIRror? - Get current mirror state
- OCR:CONFigure:COMmands:CROP? - Get current crop region
- OCR:CONFigure:COMmands:SETPixels? - Get current filter mask
- OCR:CONFigure:COMmands:KEEPixels? - Get current keep mask

OCR memory parameter commands
-----------
- OCR:MEMory:STORe *FILENAME* - Save all configurations for OCR to FILENAME.
- OCR:MEMory:DELete *FILENAME* - Delete configuration file
- OCR:MEMory:LOAD *FILENAME* - Load all configurations for OCR from FILENAME

OCR runtime parameter commands
-----------
- OCR:STATe *(RUN | STOP)* - Run/Stop ocr with already configured commands