SCPICAM
===========

SCPICAM is Raspberry Pi camera application.
Application is driven as command-line interface and all commands is designed as following by SCPI specifications.
Also this is implemented, so as to input commands by UART and TCP socket applications.

Source code is published with open source Simplified BSD license.

Command definition and function
-----------

Please reference SCPICAM_COMMAND.md.

Source code organization
------------

Source codes are divided into two folders.

- scpi-parser : SCPI parser library
- scpicam : SCPI based camera application
*main.c* - Initialize UART and TCP socket server, global SCPI variables
*scpi-camera.h* - Declare camera related properties depending on raspistill utility
*scpi-def.c* - Initialize data structure for scpicam
*scpi-def.h* - Declare MACROs and functions for scpicam
*scpi-handler.c* - Define callback functions for scpicam
*scpi-tcp.c* - Define functions by using TCP socekt
*scpi-tcp.h* - Declare functions by using TCP socket
*scpi-uart.c* - Define functions by using UART port
*scpi-uart.h* - Declare functions by using UART port
*scpi-utils.c* - Define global functions and variables
*scpi-utils.h* - Declare global functions and variables

Implementation to application
-------------

After scpicam is started, it generate two child processes(for UART input/output, for TCP socket input/output).
Each process works continuously untill scpicam is finished and if user input break command, all processes are killed.
When run command is executed, scpicam generate one process for managing raspistill.
If raspistill is finished, it is exited normally.
When raspistill is still running and stop command is executed, scpicam kill process for managing raspstill.
