#ifndef __SCPI_UTILS_H_
#define __SCPI_UTILS_H_

#define PATH_BUF_LEN	256
#define NAME_BUF_LEN	32
#define FULL_BUF_LEN	PATH_BUF_LEN + NAME_BUF_LEN

int get_absolutePath(const char * name, size_t len, char * absolute_path);

#endif // __SCPI_UTILS_H_
