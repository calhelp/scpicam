#ifndef __SCPI_UART_H_
#define __SCPI_UART_H_

#define UART_DEV_NAME	"/dev/ttyAMA0"
#define UART_RX_INTERVAL 2000 // useconds
#define UART_RX_BUF_LEN  256

#define UART_DEFAULT_TIMEOUT 60

enum UART_OPTION {
    UART_NONE,
    UART_BAUD,
    UART_RTS,
    UART_ECHO,
    UART_TIMEOUT,
    UART_RECEIVE_PACE,
    UART_TRANSMIT_PACE,
    UART_RESET,
};

enum RTS_STATE {
    RTS_ON,
    RTS_OFF,
    RTS_IBFULL,
    RTS_RFR,
};

enum ECHO_STATE {
    ECHO_OFF,
    ECHO_ON,
};

enum PACE_STATE {
    PACE_NONE,
    PACE_XON,
};

int UART_Init(void);
int UART_Receive(int fd);
int UART_Transmit(int fd, const char * data, size_t len);
int UART_Control(int option, int value);
#endif // __SCPI_UART_H_
