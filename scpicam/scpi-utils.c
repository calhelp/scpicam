#include <unistd.h>
#include <string.h>
#include "scpi-utils.h"

int get_absolutePath(const char * name, size_t len, char * absolute_path) {
    int path_len = 0;

    if (getcwd(absolute_path, FULL_BUF_LEN) == NULL)
	return -1;

    path_len = strlen(absolute_path);
    if ((path_len <= 0) || (path_len > (PATH_BUF_LEN - 2)))
	return -1;
    absolute_path[path_len] = '/';
    absolute_path[path_len + 1] = '\0';

    strncat(absolute_path, name, len);

    return strlen(absolute_path);
}
