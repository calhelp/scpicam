/*-
 * Copyright (c) 2012-2013 Jan Breuer,
 *
 * All Rights Reserved
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "scpi/scpi.h"
#include "scpi-def.h"
#include "scpi-config.h"
#include "scpi-utils.h"
#include "scpi-uart.h"
#include "scpi-tcp.h"

extern pid_t uart_cmd_pid;
extern pid_t tcp_cmd_pid;
/*----------------------------------------------*/
/* Global varables                              */
/*----------------------------------------------*/
int uart_fd = -1;
int tcp_server_fd = -1;
#if defined(MULTI_PROCESS)
pid_t uart_pid = -1;
pid_t tcp_pid = -1;
#endif

/*-------------------------------------------------------*/
/* Function : SCPI_Write                                 */
/* Description : transmitting succeeded result in execut-*/
/*               ing SCPI command to UART.               */
/* Parameters : (in) scpi_t - SCPI context               */
/*              (in) data - result string                */
/*              (in) len - result string length          */
/* Return : transmited length                            */
/*-------------------------------------------------------*/
size_t SCPI_Write(scpi_t * context, const char * data, size_t len) {
    (void) context;

    DPRINT("%s\n", data);

#if defined(MULTI_PROCESS)
    if (uart_pid == 0)
	return UART_Transmit(uart_fd, data, len);

    if (tcp_pid == 0)
	return TCP_Transmit(tcp_server_fd, data, len);
#else
    UART_Transmit(uart_fd, data, len);
    TCP_Transmit(tcp_server_fd, data, len);
#endif

    return -1;
}

/*-------------------------------------------------------*/
/* Function : SCPI_Error                                 */
/* Description : transmitting failed result in executing */
/*               SCPI command to UART.                   */
/* Parameters : (in) scpi_t - SCPI context               */
/*              (in) err - error number                  */
/* Return : transmited length                            */
/*-------------------------------------------------------*/
int SCPI_Error(scpi_t * context, int_fast16_t err) {
    (void) context;
    char tx_buffer[20];
    int tx_length;

    memset(tx_buffer, 0, 20);
    sprintf(tx_buffer, "**ERROR: %d \n", err);
    tx_length = strlen(tx_buffer);
    tx_buffer[tx_length] = '\0';

    DPRINT("**ERROR: %d, \"%s\"\r\n", (int32_t) err, SCPI_ErrorTranslate(err));

#if defined(MULTI_PROCESS)
    if (uart_pid == 0)
	return UART_Transmit(uart_fd, tx_buffer, tx_length);

    if (tcp_pid == 0)
	return TCP_Transmit(tcp_server_fd, tx_buffer, tx_length);
#else
    UART_Transmit(uart_fd, tx_buffer, tx_length);
    TCP_Transmit(tcp_server_fd, tx_buffer, tx_length);
#endif

    return -1;
}

/*-------------------------------------------------------*/
/* Function : SCPI_Control                               */
/* Description : test function                           */
/* Parameters : (in)                                     */
/*              (out)                                    */
/* Return :                                              */
/*-------------------------------------------------------*/
scpi_result_t SCPI_Control(scpi_t * context, scpi_ctrl_name_t ctrl, scpi_reg_val_t val) {
    if (SCPI_CTRL_SRQ == ctrl) {
        RPRINT("**SRQ: 0x%X (%d)\r\n", val, val);
    } else {
        RPRINT("**CTRL %02x: 0x%X (%d)\r\n", ctrl, val, val);
    }
    return SCPI_RES_OK;
}

/*-------------------------------------------------------*/
/* Function : SCPI_Reset                                 */
/* Description : test function                           */
/* Parameters : (in)                                     */
/*              (out)                                    */
/* Return :                                              */
/*-------------------------------------------------------*/
scpi_result_t SCPI_Reset(scpi_t * context) {
    RPRINT("**Reset\r\n");
    return SCPI_RES_OK;
}

/*-------------------------------------------------------*/
/* Function : SCPI_Test                                  */
/* Description : test function                           */
/* Parameters : (in)                                     */
/*              (out)                                    */
/* Return :                                              */
/*-------------------------------------------------------*/
scpi_result_t SCPI_Test(scpi_t * context) {
    RPRINT("**Test\r\n");
    return SCPI_RES_OK;
}

/*-------------------------------------------------------*/
/* Function : SCPI_Flush                                 */
/* Description : test function                           */
/* Parameters : (in)                                     */
/*              (out)                                    */
/* Return :                                              */
/*-------------------------------------------------------*/
scpi_result_t SCPI_Flush(scpi_t * context) {    
    return SCPI_RES_OK;
}

static void sigint_handler(int sig)
{
    SCPI_ResultInt(&scpi_context, EX_SIGNAL);
    abort();
}

static void sigterm_handler(int sig)
{
    if ((uart_cmd_pid == 0) || (tcp_cmd_pid == 0))
    {
	kill(0, SIGTERM);
	exit(0);
    }
}

static void zombie_handler()
{
    struct scpicam * global_conf = NULL;
    int status;
    int spid;

    spid = wait(&status);
    DPRINT("================================\n");
    DPRINT("PID         : %d\n", spid);
    DPRINT("Exit Value  : %d\n", WEXITSTATUS(status));
    DPRINT("Exit Stat   : %d\n", WIFEXITED(status));

    if (spid > 0) {
	global_conf = GetConfiguration();
	if (global_conf != NULL) {
	    if (tcp_cmd_pid > 0)
		tcp_cmd_pid = -1;
	    if (uart_cmd_pid > 0)
		uart_cmd_pid = -1;
	    global_conf->f_running = 0;
	    global_conf->f_operating = 0;
	}
    }
}

int main(int argc, char** argv) {
    (void) argc;
    (void) argv;
    int ret = 0;
    struct scpicam * global_conf = NULL;

    global_conf = GetConfiguration();
    if (global_conf == NULL)
	return (EXIT_SUCCESS);

    memset (global_conf, 0, sizeof(struct scpicam));

    if (signal(SIGINT, sigint_handler) == SIG_ERR)
        RPRINT("can't catch SIGINT\n");

    if (signal(SIGTERM, sigterm_handler) == SIG_ERR)
        RPRINT("can't catch SIGTERM\n");

    if (signal(SIGCHLD, zombie_handler) == SIG_ERR)
        RPRINT("can't catch SIGCHLD\n");

    scpi_context.user_context = NULL;
    SCPI_Init(&scpi_context);

    while (1) {
#if defined(MULTI_PROCESS)
	if (uart_pid == -1)
	    uart_pid = fork();
	if (uart_pid == 0) {
	    tcp_pid = -2;

	    // uart child process
	    if (uart_fd == -1) {
		uart_fd = UART_Init();
		if (uart_fd < 0)
		    return (EXIT_SUCCESS);
	    }

	    // Receive and Execute command from UART
	    ret = UART_Receive(uart_fd);
	}

	if (tcp_pid == -1)
	    tcp_pid = fork();
	if (tcp_pid == 0) {
	    int tcp_port = TCP_SERVER_PORT;

	    // tcp child process
	    if (global_conf->f_socket_restart) {
		if (tcp_server_fd > 0) {
		    close(tcp_server_fd);
		    tcp_server_fd = -1;
		    tcp_port = global_conf->comm.tcp.port;
		}
	    }

	    if (tcp_server_fd == -1) {
		tcp_server_fd = TCP_CreateServer(tcp_port);
		if (tcp_server_fd < 0)
		    return (EXIT_SUCCESS);
	    }

	    // Receive and Execute command from TCP socket
	    ret = TCP_Receive(tcp_server_fd);
	}

	// for decreasing CPU load
	if (uart_pid == 0) {
	    usleep(UART_RX_INTERVAL);
	} else if (tcp_pid == 0) {
	    usleep(TCP_ACCEPT_INTERVAL);
	} else {
#if defined(SCPICAM_DEBUG)
	    char smbuffer[64];
	    fgets(smbuffer, 64, stdin);
	    SCPI_Input(&scpi_context, smbuffer, strlen(smbuffer));
	    LoadConfiguration();
#endif
	    usleep(1000000);
	}
#else /* !MULTI_PROCESS */
	int tcp_port = TCP_SERVER_PORT;

	// uart child process
	if (uart_fd == -1) {
	    uart_fd = UART_Init();
#if !defined(SCPICAM_DEBUG)
	    if (uart_fd < 0)
		return (EXIT_SUCCESS);
#endif
	}

	// Receive and Execute command from UART
	ret = UART_Receive(uart_fd);

	// tcp child process
	if (global_conf->f_socket_restart)
	{
	    if (tcp_server_fd > 0) {
		close(tcp_server_fd);
		tcp_server_fd = -1;
		tcp_port = global_conf->comm.tcp.port;
	    }
	}

	if (tcp_server_fd == -1) {
	    tcp_server_fd = TCP_CreateServer(tcp_port);
#if !defined(SCPICAM_DEBUG)
	    if (tcp_server_fd < 0)
		return (EXIT_SUCCESS);
#endif
	}

	// Receive and Execute command from TCP socket
	ret = TCP_Receive(tcp_server_fd);

	usleep(2000);
#endif /* MULTI_PROCESS */
    }

    close(uart_fd);
    close(tcp_server_fd);

    return (EXIT_SUCCESS);
}

