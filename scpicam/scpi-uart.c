#include <termios.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "scpi-def.h"
#include "scpi-uart.h"
#include "scpi-config.h"

pid_t uart_cmd_pid = -1;
double startsecs = 0.0;

/*-------------------------------------------------------*/
/* Function : UART_Init                                  */
/* Description : initializing UART device                */
/* Parameters : (in)                                     */
/*              (out)                                    */
/* Return : if initializing is success                                             */
/*-------------------------------------------------------*/
int UART_Init(void) {
    int fd = -1;
    struct termios terms;

    fd = open(UART_DEV_NAME, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1)
    {
	DPRINT("Error - Unable to open UART.\n");
	return fd;
    }

#if 1
    tcgetattr(fd , &terms);
    terms.c_cflag |= B19200 | CS8 | CLOCAL | CREAD | CRTSCTS; //<Set baud rate
    terms.c_iflag |= IGNPAR;
    terms.c_iflag &= ~IXON;
    terms.c_iflag &= ~IXOFF;
    terms.c_oflag = 0;
    terms.c_lflag &= ~ECHO;
    terms.c_lflag &= ~ECHOK;

    tcflush(fd , TCIFLUSH);
    tcsetattr(fd, TCSANOW, &terms);
#else
    //get existing configuration setup
    tcgetattr(fd, &terms);

    //fcntl(deviceFD, F_SETFL, FNDELAY);
    fcntl(fd, F_SETFL, 0);

    ////set both incoming and outgoing baud rates...
    cfsetispeed(&terms, B19200);
    cfsetospeed(&terms, B19200);

    terms.c_cflag |= (CLOCAL | CREAD);

    ////8N1 (8 data bits, No parity, 1 stop bit)
    terms.c_cflag &= ~PARENB;
    terms.c_cflag &= ~CSTOPB;
    terms.c_cflag &= ~CSIZE;
    terms.c_cflag |= CS8;

    terms.c_cflag &= ~CRTSCTS;  //~CNEW_RTSCTS; //disable hardware flow control

    //use RAW unbuffered data mode (eg, not canonical mode)
    terms.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | IGNBRK);

    terms.c_iflag &= ~(IGNPAR | IXON | IXOFF | IXANY);

    //raw (unprocessed) output mode
    terms.c_oflag &= ~OPOST;

    tcflush(fd , TCIFLUSH);
    tcsetattr(fd, TCSANOW, &terms);
#endif

    return fd;
}

int UART_Receive(int fd) {
    char rx_buffer[UART_RX_BUF_LEN];
    int rx_length = 0;
    struct scpicam * global_conf = NULL;
    struct timeval tp;
    double secs;

    global_conf = GetConfiguration();
    if (global_conf == NULL)
	return -1;

    if (fd < 0)
	return -1;

    scpi_context.user_context = &fd;
    // Read up to 255 characters from the port if they are there
    //Filestream, buffer to store in, number of bytes to read (max)
    rx_length = read(fd, (void*)rx_buffer, UART_RX_BUF_LEN - 1);
    if (rx_length <= 0)
	return -1;

    //Bytes received
    rx_buffer[rx_length] = '\0';
    DPRINT("UART RX %s\n", rx_buffer);

    // execute SCPI command
    if (global_conf->f_running) {
	SCPI_Input(&scpi_context, rx_buffer, rx_length);
	if (!global_conf->f_running) {
	    global_conf->f_operating = 0;
	    if (uart_cmd_pid > 0) {
		kill(uart_cmd_pid, SIGTERM);
	    }
	} else {
	    global_conf->f_operating = 0;
	    global_conf->f_running = 0;
	}
    } else {
	SCPI_Input(&scpi_context, rx_buffer, rx_length);
	if (global_conf->f_running) {
	    global_conf->f_operating = 1;
	} else {
	    global_conf->f_operating = 0;
	}
    }

    if (global_conf->f_operating) {
	if (uart_cmd_pid < 0) {
	    gettimeofday(&tp,NULL);
	    startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;

	    uart_cmd_pid = fork();
	}

	if (uart_cmd_pid == 0) {
	    SCPI_Input(&scpi_context, rx_buffer, rx_length);
	    DPRINT("uart execution\n");
	    exit(0);
	}

	global_conf->f_operating = 0;
    }

    LoadConfiguration();

    if (uart_cmd_pid > 0) {
	int timeout;

	if (global_conf->comm.serial.f_tout)
	    timeout = global_conf->comm.serial.tout;
	else
	    timeout = UART_DEFAULT_TIMEOUT;
	gettimeofday(&tp,NULL);
	secs = tp.tv_sec + tp.tv_usec / 1000000.0;
	if (secs - startsecs > timeout) {
	    kill(uart_cmd_pid, SIGINT);
	    tcflush(fd, TCIFLUSH);
	}
    }

    return 0;
}

int UART_Transmit(int fd, const char * data, size_t len) {
    int count = -1;

    if (fd < 0)
	return -1;

    DPRINT("UART TX %s\n", data);

    //Filestream, bytes to write, number of bytes to write
    count = write(fd , data, len);
    if (count < 0)
    {
	DPRINT("UART TX error\n");
	return -1;
    }

    return count;
}

int UART_Control(int option, int value) {
    int *fd = scpi_context.user_context;
    struct termios terms;

    if (fd == NULL)
	return -1;
    if (*fd <= 0)
	return -1;

    /* Get the current terms */
    if (tcgetattr(*fd, &terms) < 0)
	return -1;

    switch (option) {
	case UART_BAUD:
	    {
		/* Set the baud rate */
		switch (value) {
		    case 1200:
			cfsetispeed(&terms,B1200);
			cfsetospeed(&terms,B1200);
			break;
		    case 2400:
			cfsetispeed(&terms,B2400);
			cfsetospeed(&terms,B2400);
			break;
		    case 4800:
			cfsetispeed(&terms,B4800);
			cfsetospeed(&terms,B4800);
			break;
		    case 9600:
			cfsetispeed(&terms,B9600);
			cfsetospeed(&terms,B9600);
			break;
		    case 19200:
			cfsetispeed(&terms,B19200);
			cfsetospeed(&terms,B19200);
			break;
		    case 38400:
			cfsetispeed(&terms,B38400);
			cfsetospeed(&terms,B38400);
			break;
		}
		break;
	    }

	case UART_RTS:
	    {
		if (value == RTS_ON)
		    terms.c_cflag |= CRTSCTS;

		if (value == RTS_OFF)
		    terms.c_cflag &= ~CRTSCTS;

		if (value == RTS_IBFULL) {}

		if (value == RTS_RFR) {}

		break;
	    }

	case UART_RESET:
	    {
		break;
	    }

	case UART_ECHO:
	    {
		if (value == ECHO_OFF) {
		    terms.c_lflag &= ~ECHO; // Don't echo
		    terms.c_lflag &= ~ECHOK; // Don't echo
		} else {
		    terms.c_lflag |= ECHO;
		    terms.c_lflag |= ECHOK;
		}
		break;
	    }

	case UART_TIMEOUT:
	    {
		return 1;
	    }

	case UART_RECEIVE_PACE:
	    {
		if (value == PACE_XON)
		    terms.c_iflag |= IXOFF;
		else
		    terms.c_iflag &= ~IXOFF;
		break;
	    }

	case UART_TRANSMIT_PACE:
	    {
		if (value == PACE_XON)
		    terms.c_iflag |= IXON;
		else
		    terms.c_iflag &= ~IXON;
		break;
	    }

	default:
	    return -1;
    }

    if (tcsetattr(*fd, TCSANOW, &terms) < 0) {
	return -1;
    }

    tcflush(*fd, TCIFLUSH);
    return 1;
}
