#ifndef __SCPI_DEF_H_
#define __SCPI_DEF_H_

#include "scpi/scpi.h"

/* Debuging flag */
//#define SCPICAM_DEBUG 1
#undef SCPICAM_DEBUG
//#define MULTI_PROCESS 1
#undef MULTI_PROCESS

#ifdef SCPICAM_DEBUG
#define	DPRINT(fmt, args...)	\
	fprintf(stdout, fmt, ##args)
#else
#define	DPRINT(fmt, args...)
#endif

#define	RPRINT(fmt, args...)	\
	fprintf(stdout, fmt, ##args)

/* Error Code */
#define EX_OK		0
#define EX_USAGE	64
#define EX_SOFTWARE	70
#define EX_SIGNAL	130

/* System Version */
#define VERSION_MAJOR	1
#define VERSION_MINER	0
#define SCPICAM_VERSION(v) sprintf(v, "V%d.%02d", VERSION_MAJOR, VERSION_MINER)
#define SCPICAM_PRODUCT_NAME	"SCPICAM"
#define SCPICAM_PRODUCT_NUMBER	1
#define SCPICAM_MANUFACTURER	"calHelp"

/* define command string */
#define STAT_PRES		"STATus:PRESet"
#define MEM_INIT		"MEMory:INITialize"
#define MEM_DEL			"MEMory:DELete"
#define MEM_LOAD		"MEMory:LOAD"
#define MEM_STOR		"MEMory:STORe"
#define PROG_STAT		"PROGram:STATe"
#define SYS_DATE		"SYSTem:DATE"
#define SYS_DATE_Q		"SYSTem:DATE?"
#define SYS_TIME		"SYSTem:TIME"
#define SYS_TIME_Q		"SYSTem:TIME?"
#define SYS_ERR_NEXT		"SYSTem:ERRor[:NEXT]?"
#define SYS_ERR_COUNT		"SYSTem:ERRor:COUNt?"
#define SYS_VER_Q		"SYSTem:VERSion?"

#define SYS_COMM_SER_RES	"SYSTem:COMMunicate:SERial:RESet"
#define SYS_COMM_SER_BAUD	"SYSTem:COMMunicate:SERial:BAUD"
#define SYS_COMM_SER_BAUD_Q	"SYSTem:COMMunicate:SERial:BAUD?"
#define SYS_COMM_SER_CONT_RTS	"SYSTem:COMMunicate:SERial:CONTrol:RTS"
#define SYS_COMM_SER_CONT_RTS_Q	"SYSTem:COMMunicate:SERial:CONTrol:RTS?"
#define SYS_COMM_SER_ECHO	"SYSTem:COMMunicate:SERial:ECHO"
#define SYS_COMM_SER_ECHO_Q	"SYSTem:COMMunicate:SERial:ECHO?"
#define SYS_COMM_SER_TOUT	"SYSTem:COMMunicate:SERial:TOUT"
#define SYS_COMM_SER_TOUT_Q	"SYSTem:COMMunicate:SERial:TOUT?"
#define SYS_COMM_SER_REC_PACE	"SYSTem:COMMunicate:SERial:RECeive:PACE"
#define SYS_COMM_SER_REC_PACE_Q	"SYSTem:COMMunicate:SERial:RECeive:PACE?"
#define SYS_COMM_SER_TRAN_PACE	"SYSTem:COMMunicate:SERial:TRANsmit:PACE"
#define SYS_COMM_SER_TRAN_PACE_Q "SYSTem:COMMunicate:SERial:TRANsmit:PACE?"
#define SYS_COMM_TCP_CONT	"SYSTem:COMMunicate:TCPip:CONTrol"
#define SYS_COMM_TCP_CONT_Q	"SYSTem:COMMunicate:TCPip:CONTrol?"
#define SYS_COMM_LAN_RES	"SYSTem:COMMunicate:LAN:RESet"
#define SYS_COMM_LAN_ADDR	"SYSTem:COMMunicate:LAN:ADDRess"
#define SYS_COMM_LAN_HOST	"SYSTem:COMMunicate:LAN:HOSTname"
#define SYS_COMM_LAN_SUB	"SYSTem:COMMunicate:LAN:SUBNetmask"
#define SYS_COMM_LAN_GATE	"SYSTem:COMMunicate:LAN:GATEway"
#define SYS_COMM_LAN_NAME1	"SYSTem:COMMunicate:LAN:NAMESERVER1"
#define SYS_COMM_LAN_NAME2	"SYSTem:COMMunicate:LAN:NAMESERVER2"

#define CAM_STAT		"CAMera:STATe"
#define CAM_MEM_STOR		"CAMera:MEMory:STORe"
#define CAM_MEM_DEL		"CAMera:MEMory:DELete"
#define CAM_MEM_LOAD		"CAMera:MEMory:LOAD"
#define CAM_CONF_OPT_WID	"CAMera:CONFigure:OPTions:WIDth"
#define CAM_CONF_OPT_HEI	"CAMera:CONFigure:OPTions:HEIght"
#define CAM_CONF_OPT_QUA	"CAMera:CONFigure:OPTions:QUAlity"
#define CAM_CONF_OPT_RAW	"CAMera:CONFigure:OPTions:RAW"
#define CAM_CONF_OPT_OUT	"CAMera:CONFigure:OPTions:OUTput"
#define CAM_CONF_OPT_VER	"CAMera:CONFigure:OPTions:VERbose"
#define CAM_CONF_OPT_TIM	"CAMera:CONFigure:OPTions:TIMeout"
#define CAM_CONF_OPT_THU	"CAMera:CONFigure:OPTions:THUmb"
#define CAM_CONF_OPT_DEM	"CAMera:CONFigure:OPTions:DEMo"
#define CAM_CONF_OPT_ENC	"CAMera:CONFigure:OPTions:ENCoding"
#define CAM_CONF_OPT_EXI	"CAMera:CONFigure:OPTions:EXIf"
#define CAM_CONF_OPT_TIME	"CAMera:CONFigure:OPTions:TIMElapse"
#define CAM_CONF_OPT_WID_Q	"CAMera:CONFigure:OPTions:WIDth?"
#define CAM_CONF_OPT_HEI_Q	"CAMera:CONFigure:OPTions:HEIght?"
#define CAM_CONF_OPT_QUA_Q	"CAMera:CONFigure:OPTions:QUAlity?"
#define CAM_CONF_OPT_OUT_Q	"CAMera:CONFigure:OPTions:OUTput?"
#define CAM_CONF_OPT_TIM_Q	"CAMera:CONFigure:OPTions:TIMeout?"
#define CAM_CONF_OPT_THU_Q	"CAMera:CONFigure:OPTions:THUmb?"
#define CAM_CONF_OPT_DEM_Q	"CAMera:CONFigure:OPTions:DEMo?"
#define CAM_CONF_OPT_ENC_Q	"CAMera:CONFigure:OPTions:ENCoding?"
#define CAM_CONF_OPT_TIME_Q	"CAMera:CONFigure:OPTions:TIMElapse?"
#define CAM_CONF_VIE_PRE	"CAMera:CONFigure:VIEw:PREview"
#define CAM_CONF_VIE_FUL	"CAMera:CONFigure:VIEw:FULlscreen"
#define CAM_CONF_VIE_NOP	"CAMera:CONFigure:VIEw:NOPreview"
#define CAM_CONF_IMA_SHA	"CAMera:CONFigure:IMAge:SHArpness"
#define CAM_CONF_IMA_CON	"CAMera:CONFigure:IMAge:CONtrast"
#define CAM_CONF_IMA_BRI	"CAMera:CONFigure:IMAge:BRIghtness"
#define CAM_CONF_IMA_SAT	"CAMera:CONFigure:IMAge:SATuration"
#define CAM_CONF_IMA_ISO	"CAMera:CONFigure:IMAge:ISO"
#define CAM_CONF_IMA_EV		"CAMera:CONFigure:IMAge:EV"
#define CAM_CONF_IMA_EXP	"CAMera:CONFigure:IMAge:EXPosure"
#define CAM_CONF_IMA_AWB	"CAMera:CONFigure:IMAge:AWB"
#define CAM_CONF_IMA_IMX	"CAMera:CONFigure:IMAge:IMXfx"
#define CAM_CONF_IMA_COL	"CAMera:CONFigure:IMAge:COLfx"
#define CAM_CONF_IMA_MET	"CAMera:CONFigure:IMAge:METering"
#define CAM_CONF_IMA_ROT	"CAMera:CONFigure:IMAge:ROTation"
#define CAM_CONF_IMA_VST	"CAMera:CONFigure:IMAge:VSTab"
#define CAM_CONF_IMA_HFL	"CAMera:CONFigure:IMAge:HFLip"
#define CAM_CONF_IMA_VFL	"CAMera:CONFigure:IMAge:VFLip"
#define CAM_CONF_IMA_SHA_Q	"CAMera:CONFigure:IMAge:SHArpness?"
#define CAM_CONF_IMA_CON_Q	"CAMera:CONFigure:IMAge:CONtrast?"
#define CAM_CONF_IMA_BRI_Q	"CAMera:CONFigure:IMAge:BRIghtness?"
#define CAM_CONF_IMA_SAT_Q	"CAMera:CONFigure:IMAge:SATuration?"
#define CAM_CONF_IMA_ISO_Q	"CAMera:CONFigure:IMAge:ISO?"
#define CAM_CONF_IMA_EV_Q	"CAMera:CONFigure:IMAge:EV?"
#define CAM_CONF_IMA_EXP_Q	"CAMera:CONFigure:IMAge:EXPosure?"
#define CAM_CONF_IMA_AWB_Q	"CAMera:CONFigure:IMAge:AWB?"
#define CAM_CONF_IMA_IMX_Q	"CAMera:CONFigure:IMAge:IMXfx?"
#define CAM_CONF_IMA_COL_Q	"CAMera:CONFigure:IMAge:COLfx?"
#define CAM_CONF_IMA_MET_Q	"CAMera:CONFigure:IMAge:METering?"
#define CAM_CONF_IMA_ROT_Q	"CAMera:CONFigure:IMAge:ROTation?"

#define OCR_STAT		"OCR:STATe"
#define OCR_MEM_STOR		"OCR:MEMory:STORe"
#define OCR_MEM_DEL		"OCR:MEMory:DELete"
#define OCR_MEM_LOAD		"OCR:MEMory:LOAD"
#define OCR_CONF_IMAGE		"OCR:CONFigure:IMAge"
#define OCR_CONF_IMAGE_Q	"OCR:CONFigure:IMAge?"
#define OCR_CONF_OPT_VERB	"OCR:CONFigure:OPTions:VERBose"
#define OCR_CONF_OPT_THR	"OCR:CONFigure:OPTions:THReshold"
#define OCR_CONF_OPT_ABST	"OCR:CONFigure:OPTions:ABSThreshold"
#define OCR_CONF_OPT_ITRT	"OCR:CONFigure:OPTions:ITRThreshold"
#define OCR_CONF_OPT_NUMP	"OCR:CONFigure:OPTions:NUMPixels"
#define OCR_CONF_OPT_IGNP	"OCR:CONFigure:OPTions:IGNPixels"
#define OCR_CONF_OPT_DIGN	"OCR:CONFigure:OPTions:DIGNumbers"
#define OCR_CONF_OPT_ONER	"OCR:CONFigure:OPTions:ONERatio"
#define OCR_CONF_OPT_MINR	"OCR:CONFigure:OPTions:MINRatio"
#define OCR_CONF_OPT_OUTI	"OCR:CONFigure:OPTions:OUTImage"
#define OCR_CONF_OPT_OUTF	"OCR:CONFigure:OPTions:OUTFormat"
#define OCR_CONF_OPT_PRO	"OCR:CONFigure:OPTions:PROcess"
#define OCR_CONF_OPT_DBGI	"OCR:CONFigure:OPTions:DBGImage"
#define OCR_CONF_OPT_DBGO	"OCR:CONFigure:OPTions:DBGOutput"
#define OCR_CONF_OPT_FORE	"OCR:CONFigure:OPTions:FOREground"
#define OCR_CONF_OPT_BACK	"OCR:CONFigure:OPTions:BACKground"
#define OCR_CONF_OPT_PRNI	"OCR:CONFigure:OPTions:PRNInfo"
#define OCR_CONF_OPT_ADJG	"OCR:CONFigure:OPTions:ADJGray"
#define OCR_CONF_OPT_LUMI	"OCR:CONFigure:OPTions:LUMInance"
#define OCR_CONF_OPT_THR_Q	"OCR:CONFigure:OPTions:THReshold?"
#define OCR_CONF_OPT_ABST_Q	"OCR:CONFigure:OPTions:ABSThreshold?"
#define OCR_CONF_OPT_ITRT_Q	"OCR:CONFigure:OPTions:ITERThreshold?"
#define OCR_CONF_OPT_NUMP_Q	"OCR:CONFigure:OPTions:NUMPixels?"
#define OCR_CONF_OPT_IGNP_Q	"OCR:CONFigure:OPTions:IGNPixels?"
#define OCR_CONF_OPT_DIGN_Q	"OCR:CONFigure:OPTions:DIGNumbers?"
#define OCR_CONF_OPT_ONER_Q	"OCR:CONFigure:OPTions:ONERatio?"
#define OCR_CONF_OPT_MINR_Q	"OCR:CONFigure:OPTions:MINRatio?"
#define OCR_CONF_OPT_OUTI_Q	"OCR:CONFigure:OPTions:OUTImage?"
#define OCR_CONF_OPT_OUTF_Q	"OCR:CONFigure:OPTions:OUTFormat?"
#define OCR_CONF_OPT_DBGI_Q	"OCR:CONFigure:OPTions:DBGImage?"
#define OCR_CONF_OPT_FORE_Q	"OCR:CONFigure:OPTions:FOREground?"
#define OCR_CONF_OPT_BACK_Q	"OCR:CONFigure:OPTions:BACKground?"
#define OCR_CONF_OPT_LUMI_Q	"OCR:CONFigure:OPTions:LUMInance?"
#define OCR_CONF_COM_DILA	"OCR:CONFigure:COMmands:DILAtion"
#define OCR_CONF_COM_EROS	"OCR:CONFigure:COMmands:EROSion"
#define OCR_CONF_COM_CLOS	"OCR:CONFigure:COMmands:CLOSing"
#define OCR_CONF_COM_OPEN	"OCR:CONFigure:COMmands:OPENing"
#define OCR_CONF_COM_RMVI	"OCR:CONFigure:COMmands:RMVIsolated"
#define OCR_CONF_COM_GRAY	"OCR:CONFigure:COMmands:GRAYscale"
#define OCR_CONF_COM_MONO	"OCR:CONFigure:COMmands:MONO"
#define OCR_CONF_COM_INV	"OCR:CONFigure:COMmands:INVert"
#define OCR_CONF_COM_STRE	"OCR:CONFigure:COMmands:STREtch"
#define OCR_CONF_COM_DYN	"OCR:CONFigure:COMmands:DYNamic"
#define OCR_CONF_COM_RGB	"OCR:CONFigure:COMmands:RGB"
#define OCR_CONF_COM_RTH	"OCR:CONFigure:COMmands:RTHreshold"
#define OCR_CONF_COM_GTH	"OCR:CONFigure:COMmands:GTHreshold"
#define OCR_CONF_COM_BTH	"OCR:CONFigure:COMmands:BTHreshold"
#define OCR_CONF_COM_WHI	"OCR:CONFigure:COMmands:WHIte"
#define OCR_CONF_COM_SHE	"OCR:CONFigure:COMmands:SHEar"
#define OCR_CONF_COM_ROT	"OCR:CONFigure:COMmands:ROTate"
#define OCR_CONF_COM_MIR	"OCR:CONFigure:COMmands:MIRror"
#define OCR_CONF_COM_CROP	"OCR:CONFigure:COMmands:CROP"
#define OCR_CONF_COM_SETP	"OCR:CONFigure:COMmands:SETPixels"
#define OCR_CONF_COM_KEEP	"OCR:CONFigure:COMmands:KEEPixels"
#define OCR_CONF_COM_CLOS_Q	"OCR:CONFigure:COMmands:CLOSing?"
#define OCR_CONF_COM_OPEN_Q	"OCR:CONFigure:COMmands:OPENing?"
#define OCR_CONF_COM_STRE_Q	"OCR:CONFigure:COMmands:STREtch?"
#define OCR_CONF_COM_DYN_Q	"OCR:CONFigure:COMmands:DYNamic?"
#define OCR_CONF_COM_WHI_Q	"OCR:CONFigure:COMmands:WHIte?"
#define OCR_CONF_COM_SHE_Q	"OCR:CONFigure:COMmands:SHEar?"
#define OCR_CONF_COM_ROT_Q	"OCR:CONFigure:COMmands:ROTate?"
#define OCR_CONF_COM_MIR_Q	"OCR:CONFigure:COMmands:MIRror?"
#define OCR_CONF_COM_CROP_Q	"OCR:CONFigure:COMmands:CROP?"
#define OCR_CONF_COM_SETP_Q	"OCR:CONFigure:COMmands:SETPixels?"
#define OCR_CONF_COM_KEEP_Q	"OCR:CONFigure:COMmands:KEEPixels?"

/* declare SCPI global context */
extern scpi_t scpi_context;

/* declare SCPI global handlers */
size_t SCPI_Write(scpi_t * context, const char * data, size_t len);
int SCPI_Error(scpi_t * context, int_fast16_t err);
scpi_result_t SCPI_Control(scpi_t * context, scpi_ctrl_name_t ctrl, scpi_reg_val_t val);
scpi_result_t SCPI_Reset(scpi_t * context);
scpi_result_t SCPI_Test(scpi_t * context);
scpi_result_t SCPI_Flush(scpi_t * context);

scpi_result_t SCPI_RaspberryPreset(scpi_t * context);
scpi_result_t SCPI_RaspberryVersionQ(scpi_t * context);
scpi_result_t SCPI_RaspberryConfigInitialize(scpi_t * context);
scpi_result_t SCPI_RaspberryConfigDelete(scpi_t * context);
scpi_result_t SCPI_RaspberryConfigLoad(scpi_t * context);
scpi_result_t SCPI_RaspberryConfigStore(scpi_t * context);
scpi_result_t SCPI_RaspberryProgramState(scpi_t * context);
scpi_result_t SCPI_RaspberryCameraState(scpi_t * context);
scpi_result_t SCPI_RaspberryOcrState(scpi_t * context);

scpi_result_t LoadConfiguration(void);
struct scpicam * GetConfiguration(void);

/* SCPICAM camera handlers */
scpi_result_t SCPI_CameraConfigureOptionsWidth(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsWidthQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsHeight(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsHeightQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsQuality(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsQualityQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsRaw(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsOutput(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsOutputQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsVerbose(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsTimeout(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsTimeoutQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsThumb(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsThumbQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsDemo(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsDemoQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsEncoding(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsEncodingQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsExif(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsTimelapse(scpi_t * context);
scpi_result_t SCPI_CameraConfigureOptionsTimelapseQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureViewPreview(scpi_t * context);
scpi_result_t SCPI_CameraConfigureViewFullscreen(scpi_t * context);
scpi_result_t SCPI_CameraConfigureViewNopreview(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageSharpness(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageContrast(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageBrightness(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageSaturation(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageIso(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageEv(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageExposure(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageAwb(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageImxfx(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageColfx(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageMetering(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageRotation(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageSharpnessQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageContrastQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageBrightnessQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageSaturationQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageIsoQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageEvQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageExposureQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageAwbQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageImxfxQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageColfxQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageMeteringQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageRotationQ(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageVstab(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageHflip(scpi_t * context);
scpi_result_t SCPI_CameraConfigureImageVflip(scpi_t * context);
scpi_result_t SCPI_CameraMemoryStore(scpi_t * context);
scpi_result_t SCPI_CameraMemoryDelete(scpi_t * context);
scpi_result_t SCPI_CameraMemoryLoad(scpi_t * context);

/* SCPICAM ocr handlers */
scpi_result_t SCPI_OcrConfigureImage(scpi_t * context);
scpi_result_t SCPI_OcrConfigureImageQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsVerbose(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsThreshold(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsAbsoluteThreshold(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsIterThreshold(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsNumberPixels(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsIgnorePixels(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsNumberDigits(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsOneRatio(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsMinusRatio(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsOutputImage(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsOutputFormat(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsProcessOnly(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsDebugImage(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsDebugOutput(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsForeground(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsBackground(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsPrintInfo(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsAdjustGray(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsLuminance(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsVersionQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsThresholdQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsAbsoluteThresholdQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsIterThresholdQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsNumberPixelsQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsIgnorePixelsQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsNumberDigitsQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsOneRatioQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsMinusRatioQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsOutputImageQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsOutputFormatQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsDebugImageQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsForegroundQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsBackgroundQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureOptionsLuminanceQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsDilation(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsErosion(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsClosing(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsOpening(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsRemoveIsolated(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsMakeMono(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsGrayScale(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsInvert(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsStretch(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsDynamic(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsRgb(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsRthreshold(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsGthreshold(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsBthreshold(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsWhiteBorder(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsShear(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsRotate(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsMirror(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsCrop(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsSetPixels(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsKeepPixels(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsClosingQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsOpeningQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsStretchQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsDynamicQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsWhiteBorderQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsShearQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsRotateQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsMirrorQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsCropQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsSetPixelsQ(scpi_t * context);
scpi_result_t SCPI_OcrConfigureCommandsKeepPixelsQ(scpi_t * context);
scpi_result_t SCPI_OcrMemoryLoad(scpi_t * context);
scpi_result_t SCPI_OcrMemoryStore(scpi_t * context);
scpi_result_t SCPI_OcrMemoryDelete(scpi_t * context);

scpi_result_t SCPI_SystemIdnQ(scpi_t * context);
scpi_result_t SCPI_SystemDate(scpi_t * context);
scpi_result_t SCPI_SystemDateQ(scpi_t * context);
scpi_result_t SCPI_SystemTime(scpi_t * context);
scpi_result_t SCPI_SystemTimeQ(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialReset(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialBaud(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialBaudQ(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialCountrolRts(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialCountrolRtsQ(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialEcho(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialEchoQ(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialTimeout(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialTimeoutQ(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialReceivePace(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialReceivePaceQ(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialTransmitPace(scpi_t * context);
scpi_result_t SCPI_CommunicateSerialTransmitPaceQ(scpi_t * context);
scpi_result_t SCPI_CommunicateTcpControl(scpi_t * context);
scpi_result_t SCPI_CommunicateTcpControlQ(scpi_t * context);
scpi_result_t SCPI_CommunicateLanReset(scpi_t * context);
scpi_result_t SCPI_CommunicateLanAddress(scpi_t * context);
scpi_result_t SCPI_CommunicateLanHostname(scpi_t * context);
scpi_result_t SCPI_CommunicateLanSubnet(scpi_t * context);
scpi_result_t SCPI_CommunicateLanGateway(scpi_t * context);
scpi_result_t SCPI_CommunicateLanNameserver1(scpi_t * context);
scpi_result_t SCPI_CommunicateLanNameserver2(scpi_t * context);
#endif // __SCPI_DEF_H_

