/*-
 * Copyright (c) 2012-2013 Jan Breuer,
 *
 * All Rights Reserved
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "scpi/scpi.h"
#include "scpi-def.h"

static const scpi_command_t scpi_commands[] = {
#if 0
    {.pattern = "*CLS", .callback = SCPI_CoreCls,},
    {.pattern = "*ESE", .callback = SCPI_CoreEse,},
    {.pattern = "*ESE?", .callback = SCPI_CoreEseQ,},
    {.pattern = "*ESR?", .callback = SCPI_CoreEsrQ,},
    {.pattern = "*IDN?", .callback = SCPI_CoreIdnQ,},
    {.pattern = "*OPC", .callback = SCPI_CoreOpc,},
    {.pattern = "*OPC?", .callback = SCPI_CoreOpcQ,},
    {.pattern = "*RST", .callback = SCPI_CoreRst,},
    {.pattern = "*SRE", .callback = SCPI_CoreSre,},
    {.pattern = "*SRE?", .callback = SCPI_CoreSreQ,},
    {.pattern = "*STB?", .callback = SCPI_CoreStbQ,},
    {.pattern = "*TST?", .callback = SCPI_CoreTstQ,},
    {.pattern = "*WAI", .callback = SCPI_CoreWai,},

    {.pattern = "STATus:QUEStionable[:EVENt]?", .callback = SCPI_StatusQuestionableEventQ,},
    {.pattern = "STATus:QUEStionable:ENABle", .callback = SCPI_StatusQuestionableEnable,},
    {.pattern = "STATus:QUEStionable:ENABle?", .callback = SCPI_StatusQuestionableEnableQ,},
#endif

    /* System Commands*/
    {.pattern = "*IDN?", .callback = SCPI_SystemIdnQ,},

    {.pattern = SYS_DATE, .callback = SCPI_SystemDate,},
    {.pattern = SYS_DATE_Q, .callback = SCPI_SystemDateQ,},
    {.pattern = SYS_TIME, .callback = SCPI_SystemTime,},
    {.pattern = SYS_TIME_Q, .callback = SCPI_SystemTimeQ,},
    {.pattern = SYS_ERR_NEXT, .callback = SCPI_SystemErrorNextQ,},
    {.pattern = SYS_ERR_COUNT, .callback = SCPI_SystemErrorCountQ,},
    {.pattern = SYS_VER_Q, .callback = SCPI_RaspberryVersionQ,},
    {.pattern = STAT_PRES, .callback = SCPI_RaspberryPreset,},
    {.pattern = MEM_INIT, .callback = SCPI_RaspberryConfigInitialize,},
    {.pattern = MEM_DEL, .callback = SCPI_RaspberryConfigDelete,},
    {.pattern = MEM_LOAD, .callback = SCPI_RaspberryConfigLoad,},
    {.pattern = MEM_STOR, .callback = SCPI_RaspberryConfigStore,},
    {.pattern = PROG_STAT, .callback = SCPI_RaspberryProgramState,},
    {.pattern = CAM_STAT, .callback = SCPI_RaspberryCameraState,},
    {.pattern = OCR_STAT, .callback = SCPI_RaspberryOcrState,},

    /* Communicate Commands */
    /* For Serial port */
    {.pattern = SYS_COMM_SER_RES, .callback = SCPI_CommunicateSerialReset,},
    {.pattern = SYS_COMM_SER_BAUD, .callback = SCPI_CommunicateSerialBaud,},
    {.pattern = SYS_COMM_SER_BAUD_Q, .callback = SCPI_CommunicateSerialBaudQ,},
    {.pattern = SYS_COMM_SER_CONT_RTS, .callback = SCPI_CommunicateSerialCountrolRts,},
    {.pattern = SYS_COMM_SER_CONT_RTS_Q, .callback = SCPI_CommunicateSerialCountrolRtsQ,},
    {.pattern = SYS_COMM_SER_ECHO, .callback = SCPI_CommunicateSerialEcho,},
    {.pattern = SYS_COMM_SER_ECHO_Q, .callback = SCPI_CommunicateSerialEchoQ,},
    {.pattern = SYS_COMM_SER_TOUT, .callback = SCPI_CommunicateSerialTimeout,},
    {.pattern = SYS_COMM_SER_TOUT_Q, .callback = SCPI_CommunicateSerialTimeoutQ,},
    {.pattern = SYS_COMM_SER_REC_PACE, .callback = SCPI_CommunicateSerialReceivePace,},
    {.pattern = SYS_COMM_SER_REC_PACE_Q, .callback = SCPI_CommunicateSerialReceivePaceQ,},
    {.pattern = SYS_COMM_SER_TRAN_PACE, .callback = SCPI_CommunicateSerialTransmitPace,},
    {.pattern = SYS_COMM_SER_TRAN_PACE_Q, .callback = SCPI_CommunicateSerialTransmitPaceQ,},

    /* For TCP-IP socket */
    {.pattern = SYS_COMM_TCP_CONT, .callback = SCPI_CommunicateTcpControl,},
    {.pattern = SYS_COMM_TCP_CONT_Q, .callback = SCPI_CommunicateTcpControlQ,},

    /* For configuring LAN */
    {.pattern = SYS_COMM_LAN_RES, .callback = SCPI_CommunicateLanReset,},
    {.pattern = SYS_COMM_LAN_ADDR, .callback = SCPI_CommunicateLanAddress,},
    {.pattern = SYS_COMM_LAN_HOST, .callback = SCPI_CommunicateLanHostname,},
    {.pattern = SYS_COMM_LAN_SUB, .callback = SCPI_CommunicateLanSubnet,},
    {.pattern = SYS_COMM_LAN_GATE, .callback = SCPI_CommunicateLanGateway,},
    {.pattern = SYS_COMM_LAN_NAME1, .callback = SCPI_CommunicateLanNameserver1,},
    {.pattern = SYS_COMM_LAN_NAME2, .callback = SCPI_CommunicateLanNameserver2,},

    /* Camera Commands*/
    /* For configuring options */
    {.pattern = CAM_CONF_OPT_WID, .callback = SCPI_CameraConfigureOptionsWidth,},
    {.pattern = CAM_CONF_OPT_WID_Q, .callback = SCPI_CameraConfigureOptionsWidthQ,},
    {.pattern = CAM_CONF_OPT_HEI, .callback = SCPI_CameraConfigureOptionsHeight,},
    {.pattern = CAM_CONF_OPT_HEI_Q, .callback = SCPI_CameraConfigureOptionsHeightQ,},
    {.pattern = CAM_CONF_OPT_QUA, .callback = SCPI_CameraConfigureOptionsQuality,},
    {.pattern = CAM_CONF_OPT_QUA_Q, .callback = SCPI_CameraConfigureOptionsQualityQ,},
    {.pattern = CAM_CONF_OPT_RAW, .callback = SCPI_CameraConfigureOptionsRaw,},
    {.pattern = CAM_CONF_OPT_OUT, .callback = SCPI_CameraConfigureOptionsOutput,},
    {.pattern = CAM_CONF_OPT_OUT_Q, .callback = SCPI_CameraConfigureOptionsOutputQ,},
    {.pattern = CAM_CONF_OPT_VER, .callback = SCPI_CameraConfigureOptionsVerbose,},
    {.pattern = CAM_CONF_OPT_TIM, .callback = SCPI_CameraConfigureOptionsTimeout,},
    {.pattern = CAM_CONF_OPT_TIM_Q, .callback = SCPI_CameraConfigureOptionsTimeoutQ,},
    {.pattern = CAM_CONF_OPT_THU, .callback = SCPI_CameraConfigureOptionsThumb,},
    {.pattern = CAM_CONF_OPT_THU_Q, .callback = SCPI_CameraConfigureOptionsThumbQ,},
    {.pattern = CAM_CONF_OPT_DEM, .callback = SCPI_CameraConfigureOptionsDemo,},
    {.pattern = CAM_CONF_OPT_DEM_Q, .callback = SCPI_CameraConfigureOptionsDemoQ,},
    {.pattern = CAM_CONF_OPT_ENC, .callback = SCPI_CameraConfigureOptionsEncoding,},
    {.pattern = CAM_CONF_OPT_ENC_Q, .callback = SCPI_CameraConfigureOptionsEncodingQ,},
    {.pattern = CAM_CONF_OPT_EXI, .callback = SCPI_CameraConfigureOptionsExif,},
    {.pattern = CAM_CONF_OPT_TIME, .callback = SCPI_CameraConfigureOptionsTimelapse,},
    {.pattern = CAM_CONF_OPT_TIME_Q, .callback = SCPI_CameraConfigureOptionsTimelapseQ,},

    /* For configuring preview */
    {.pattern = CAM_CONF_VIE_PRE, .callback = SCPI_CameraConfigureViewPreview,},
    {.pattern = CAM_CONF_VIE_FUL, .callback = SCPI_CameraConfigureViewFullscreen,},
    {.pattern = CAM_CONF_VIE_NOP, .callback = SCPI_CameraConfigureViewNopreview,},

    /* For tunning image */
    {.pattern = CAM_CONF_IMA_SHA, .callback = SCPI_CameraConfigureImageSharpness,},
    {.pattern = CAM_CONF_IMA_CON, .callback = SCPI_CameraConfigureImageContrast,},
    {.pattern = CAM_CONF_IMA_BRI, .callback = SCPI_CameraConfigureImageBrightness,},
    {.pattern = CAM_CONF_IMA_SAT, .callback = SCPI_CameraConfigureImageSaturation,},
    {.pattern = CAM_CONF_IMA_ISO, .callback = SCPI_CameraConfigureImageIso,},
    {.pattern = CAM_CONF_IMA_EV, .callback = SCPI_CameraConfigureImageEv,},
    {.pattern = CAM_CONF_IMA_EXP, .callback = SCPI_CameraConfigureImageExposure,},
    {.pattern = CAM_CONF_IMA_AWB, .callback = SCPI_CameraConfigureImageAwb,},
    {.pattern = CAM_CONF_IMA_IMX, .callback = SCPI_CameraConfigureImageImxfx,},
    {.pattern = CAM_CONF_IMA_COL, .callback = SCPI_CameraConfigureImageColfx,},
    {.pattern = CAM_CONF_IMA_MET, .callback = SCPI_CameraConfigureImageMetering,},
    {.pattern = CAM_CONF_IMA_ROT, .callback = SCPI_CameraConfigureImageRotation,},
    {.pattern = CAM_CONF_IMA_SHA_Q, .callback = SCPI_CameraConfigureImageSharpnessQ,},
    {.pattern = CAM_CONF_IMA_CON_Q, .callback = SCPI_CameraConfigureImageContrastQ,},
    {.pattern = CAM_CONF_IMA_BRI_Q, .callback = SCPI_CameraConfigureImageBrightnessQ,},
    {.pattern = CAM_CONF_IMA_SAT_Q, .callback = SCPI_CameraConfigureImageSaturationQ,},
    {.pattern = CAM_CONF_IMA_ISO_Q, .callback = SCPI_CameraConfigureImageIsoQ,},
    {.pattern = CAM_CONF_IMA_EV_Q, .callback = SCPI_CameraConfigureImageEvQ,},
    {.pattern = CAM_CONF_IMA_EXP_Q, .callback = SCPI_CameraConfigureImageExposureQ,},
    {.pattern = CAM_CONF_IMA_AWB_Q, .callback = SCPI_CameraConfigureImageAwbQ,},
    {.pattern = CAM_CONF_IMA_IMX_Q, .callback = SCPI_CameraConfigureImageImxfxQ,},
    {.pattern = CAM_CONF_IMA_COL_Q, .callback = SCPI_CameraConfigureImageColfxQ,},
    {.pattern = CAM_CONF_IMA_MET_Q, .callback = SCPI_CameraConfigureImageMeteringQ,},
    {.pattern = CAM_CONF_IMA_ROT_Q, .callback = SCPI_CameraConfigureImageRotationQ,},
    {.pattern = CAM_CONF_IMA_VST, .callback = SCPI_CameraConfigureImageVstab,},
    {.pattern = CAM_CONF_IMA_HFL, .callback = SCPI_CameraConfigureImageHflip,},
    {.pattern = CAM_CONF_IMA_VFL, .callback = SCPI_CameraConfigureImageVflip,},

    /* For managing configuration */
    {.pattern = CAM_MEM_STOR, .callback = SCPI_CameraMemoryStore,},
    {.pattern = CAM_MEM_DEL, .callback = SCPI_CameraMemoryDelete,},
    {.pattern = CAM_MEM_LOAD, .callback = SCPI_CameraMemoryLoad,},


    /* OCR Commands*/
    /* For configuring image name */
    {.pattern = OCR_CONF_IMAGE, .callback = SCPI_OcrConfigureImage,},
    {.pattern = OCR_CONF_IMAGE_Q, .callback = SCPI_OcrConfigureImageQ,},

    /* For configuring options */
    {.pattern = OCR_CONF_OPT_VERB, .callback = SCPI_OcrConfigureOptionsVerbose,},
    {.pattern = OCR_CONF_OPT_THR, .callback = SCPI_OcrConfigureOptionsThreshold,},
    {.pattern = OCR_CONF_OPT_ABST, .callback = SCPI_OcrConfigureOptionsAbsoluteThreshold,},
    {.pattern = OCR_CONF_OPT_ITRT, .callback = SCPI_OcrConfigureOptionsIterThreshold,},
    {.pattern = OCR_CONF_OPT_NUMP, .callback = SCPI_OcrConfigureOptionsNumberPixels,},
    {.pattern = OCR_CONF_OPT_IGNP, .callback = SCPI_OcrConfigureOptionsIgnorePixels,},
    {.pattern = OCR_CONF_OPT_DIGN, .callback = SCPI_OcrConfigureOptionsNumberDigits,},
    {.pattern = OCR_CONF_OPT_ONER, .callback = SCPI_OcrConfigureOptionsOneRatio,},
    {.pattern = OCR_CONF_OPT_MINR, .callback = SCPI_OcrConfigureOptionsMinusRatio,},
    {.pattern = OCR_CONF_OPT_OUTI, .callback = SCPI_OcrConfigureOptionsOutputImage,},
    {.pattern = OCR_CONF_OPT_OUTF, .callback = SCPI_OcrConfigureOptionsOutputFormat,},
    {.pattern = OCR_CONF_OPT_PRO, .callback = SCPI_OcrConfigureOptionsProcessOnly,},
    {.pattern = OCR_CONF_OPT_DBGI, .callback = SCPI_OcrConfigureOptionsDebugImage,},
    {.pattern = OCR_CONF_OPT_DBGO, .callback = SCPI_OcrConfigureOptionsDebugOutput,},
    {.pattern = OCR_CONF_OPT_FORE, .callback = SCPI_OcrConfigureOptionsForeground,},
    {.pattern = OCR_CONF_OPT_BACK, .callback = SCPI_OcrConfigureOptionsBackground,},
    {.pattern = OCR_CONF_OPT_PRNI, .callback = SCPI_OcrConfigureOptionsPrintInfo,},
    {.pattern = OCR_CONF_OPT_ADJG, .callback = SCPI_OcrConfigureOptionsAdjustGray,},
    {.pattern = OCR_CONF_OPT_LUMI, .callback = SCPI_OcrConfigureOptionsLuminance,},
    {.pattern = OCR_CONF_OPT_THR_Q, .callback = SCPI_OcrConfigureOptionsThresholdQ,},
    {.pattern = OCR_CONF_OPT_ABST_Q, .callback = SCPI_OcrConfigureOptionsAbsoluteThresholdQ,},
    {.pattern = OCR_CONF_OPT_ITRT_Q, .callback = SCPI_OcrConfigureOptionsIterThresholdQ,},
    {.pattern = OCR_CONF_OPT_NUMP_Q, .callback = SCPI_OcrConfigureOptionsNumberPixelsQ,},
    {.pattern = OCR_CONF_OPT_IGNP_Q, .callback = SCPI_OcrConfigureOptionsIgnorePixelsQ,},
    {.pattern = OCR_CONF_OPT_DIGN_Q, .callback = SCPI_OcrConfigureOptionsNumberDigitsQ,},
    {.pattern = OCR_CONF_OPT_ONER_Q, .callback = SCPI_OcrConfigureOptionsOneRatioQ,},
    {.pattern = OCR_CONF_OPT_MINR_Q, .callback = SCPI_OcrConfigureOptionsMinusRatioQ,},
    {.pattern = OCR_CONF_OPT_OUTI_Q, .callback = SCPI_OcrConfigureOptionsOutputImageQ,},
    {.pattern = OCR_CONF_OPT_OUTF_Q, .callback = SCPI_OcrConfigureOptionsOutputFormatQ,},
    {.pattern = OCR_CONF_OPT_FORE_Q, .callback = SCPI_OcrConfigureOptionsForegroundQ,},
    {.pattern = OCR_CONF_OPT_BACK_Q, .callback = SCPI_OcrConfigureOptionsBackgroundQ,},
    {.pattern = OCR_CONF_OPT_LUMI_Q, .callback = SCPI_OcrConfigureOptionsLuminanceQ,},

    /* For configuring commands */
    {.pattern = OCR_CONF_COM_DILA, .callback = SCPI_OcrConfigureCommandsDilation,},
    {.pattern = OCR_CONF_COM_EROS, .callback = SCPI_OcrConfigureCommandsErosion,},
    {.pattern = OCR_CONF_COM_CLOS, .callback = SCPI_OcrConfigureCommandsClosing,},
    {.pattern = OCR_CONF_COM_OPEN, .callback = SCPI_OcrConfigureCommandsOpening,},
    {.pattern = OCR_CONF_COM_RMVI, .callback = SCPI_OcrConfigureCommandsRemoveIsolated,},
    {.pattern = OCR_CONF_COM_GRAY, .callback = SCPI_OcrConfigureCommandsMakeMono,},
    {.pattern = OCR_CONF_COM_MONO, .callback = SCPI_OcrConfigureCommandsGrayScale,},
    {.pattern = OCR_CONF_COM_INV, .callback = SCPI_OcrConfigureCommandsInvert,},
    {.pattern = OCR_CONF_COM_STRE, .callback = SCPI_OcrConfigureCommandsStretch,},
    {.pattern = OCR_CONF_COM_DYN, .callback = SCPI_OcrConfigureCommandsDynamic,},
    {.pattern = OCR_CONF_COM_RGB, .callback = SCPI_OcrConfigureCommandsRgb,},
    {.pattern = OCR_CONF_COM_RTH, .callback = SCPI_OcrConfigureCommandsRthreshold,},
    {.pattern = OCR_CONF_COM_GTH, .callback = SCPI_OcrConfigureCommandsGthreshold,},
    {.pattern = OCR_CONF_COM_BTH, .callback = SCPI_OcrConfigureCommandsBthreshold,},
    {.pattern = OCR_CONF_COM_WHI, .callback = SCPI_OcrConfigureCommandsWhiteBorder,},
    {.pattern = OCR_CONF_COM_SHE, .callback = SCPI_OcrConfigureCommandsShear,},
    {.pattern = OCR_CONF_COM_ROT, .callback = SCPI_OcrConfigureCommandsRotate,},
    {.pattern = OCR_CONF_COM_MIR, .callback = SCPI_OcrConfigureCommandsMirror,},
    {.pattern = OCR_CONF_COM_CROP, .callback = SCPI_OcrConfigureCommandsCrop,},
    {.pattern = OCR_CONF_COM_SETP, .callback = SCPI_OcrConfigureCommandsSetPixels,},
    {.pattern = OCR_CONF_COM_KEEP, .callback = SCPI_OcrConfigureCommandsKeepPixels,},
    {.pattern = OCR_CONF_COM_CLOS_Q, .callback = SCPI_OcrConfigureCommandsClosingQ,},
    {.pattern = OCR_CONF_COM_OPEN_Q, .callback = SCPI_OcrConfigureCommandsOpeningQ,},
    {.pattern = OCR_CONF_COM_STRE_Q, .callback = SCPI_OcrConfigureCommandsStretchQ,},
    {.pattern = OCR_CONF_COM_DYN_Q, .callback = SCPI_OcrConfigureCommandsDynamicQ,},
    {.pattern = OCR_CONF_COM_WHI_Q, .callback = SCPI_OcrConfigureCommandsWhiteBorderQ,},
    {.pattern = OCR_CONF_COM_SHE_Q, .callback = SCPI_OcrConfigureCommandsShearQ,},
    {.pattern = OCR_CONF_COM_ROT_Q, .callback = SCPI_OcrConfigureCommandsRotateQ,},
    {.pattern = OCR_CONF_COM_MIR_Q, .callback = SCPI_OcrConfigureCommandsMirrorQ,},
    {.pattern = OCR_CONF_COM_CROP_Q, .callback = SCPI_OcrConfigureCommandsCropQ,},
    {.pattern = OCR_CONF_COM_SETP_Q, .callback = SCPI_OcrConfigureCommandsSetPixelsQ,},
    {.pattern = OCR_CONF_COM_KEEP_Q, .callback = SCPI_OcrConfigureCommandsKeepPixelsQ,},

    /* For managing configuration */
    {.pattern = OCR_MEM_LOAD, .callback = SCPI_OcrMemoryLoad,},
    {.pattern = OCR_MEM_STOR, .callback = SCPI_OcrMemoryStore,},
    {.pattern = OCR_MEM_DEL, .callback = SCPI_OcrMemoryDelete,},

    SCPI_CMD_LIST_END
};

static scpi_interface_t scpi_interface = {
    .error = SCPI_Error,
    .write = SCPI_Write,
    .control = SCPI_Control,
    .flush = SCPI_Flush,
    .reset = SCPI_Reset,
    .test = SCPI_Test,
};

#define SCPI_INPUT_BUFFER_LENGTH 256
static char scpi_input_buffer[SCPI_INPUT_BUFFER_LENGTH];

static scpi_reg_val_t scpi_regs[SCPI_REG_COUNT];


scpi_t scpi_context = {
    .cmdlist = scpi_commands,
    .buffer = {
        .length = SCPI_INPUT_BUFFER_LENGTH,
        .data = scpi_input_buffer,
    },
    .interface = &scpi_interface,
    .registers = scpi_regs,
    .units = scpi_units_def,
    .special_numbers = scpi_special_numbers_def,
    .idn = {"scpicam", "calhelp-2014", NULL, "01-20"},
};
