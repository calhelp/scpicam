#include <time.h>
#include <netinet/in.h>

#include "scpi/scpi.h"
#include "scpi-utils.h"

#define NETIF_NAME	"eth2"

typedef struct _thumb {
    int x;
    int y;
    int quality;
} THUMB;

typedef struct _rect {
    int x;
    int y;
    int w;
    int h;
} RECT;

typedef struct _colfx {
    int u;
    int v;
} COLFX;

struct raspistill_options {
    unsigned f_width:1;
    unsigned f_height:1;
    unsigned f_quality:1;
    unsigned f_raw:1;
    unsigned f_output:1;
    unsigned f_verbose:1;
    unsigned f_timeout:1;
    unsigned f_thumb:1;
    unsigned f_timelapse:1;
    unsigned f_demo:1;
    unsigned f_encoding:1;

    int width;
    int height;
    int quality;
    char output[FULL_BUF_LEN];
    int timeout;
    THUMB thumb;
    int timelapse;
    int demo;
    int encoding;
};

struct raspistill_view {
    unsigned f_preview:1;
    unsigned f_full:1;

    RECT preview;
};

struct raspistill_image {
    unsigned f_sharpness:1;
    unsigned f_contrast:1;
    unsigned f_brightness:1;
    unsigned f_saturation:1;
    unsigned f_iso:1;
    unsigned f_ev:1;
    unsigned f_exposure:1;
    unsigned f_awb:1;
    unsigned f_imxfx:1;
    unsigned f_colfx:1;
    unsigned f_metering:1;
    unsigned f_rotation:1;
    unsigned f_vstab:1;
    unsigned f_hflip:1;
    unsigned f_vflip:1;

    int sharpness;
    int contrast;
    int brightness;
    int saturation;
    int iso;
    int ev;
    int exposure;
    int awb;
    int imxfx;
    COLFX colfx;
    int metering;
    int rotation;
};

struct raspistill {
    struct raspistill_options	options;
    struct raspistill_view	view;
    struct raspistill_image	image;
    char config_name[FULL_BUF_LEN];
};

struct ssocr_options {
    unsigned f_verbose:1;
    unsigned f_threshold:1;
    unsigned f_absolute:1;
    unsigned f_iter:1;
    unsigned f_numberPixels:1;
    unsigned f_ignorePixels:1;
    unsigned f_numberDigits:1;
    unsigned f_oneRatio:1;
    unsigned f_minusRatio:1;
    unsigned f_outputImage:1;
    unsigned f_outputFormat:1;
    unsigned f_processOnly:1;
    unsigned f_debugImage:1;
    unsigned f_debugOutput:1;
    unsigned f_foreground:1;
    unsigned f_background:1;
    unsigned f_printInfo:1;
    unsigned f_adjustGray:1;
    unsigned f_luminance:1;

    int threshold;
    int numberPixels;
    int ignorePixels;
    int numberDigits;
    int oneRatio;
    int minusRatio;
    char outputImage[FULL_BUF_LEN];
    int outputFormat;
    char debugImage[FULL_BUF_LEN];
    int foreground;
    int background;
    int luminance;
};

struct ssocr_commands {
    unsigned f_dilation:1;
    unsigned f_erosion:1;
    unsigned f_closing:1;
    unsigned f_opening:1;
    unsigned f_removeIsolated:1;
    unsigned f_makeMono:1;
    unsigned f_grayscale:1;
    unsigned f_invert:1;
    unsigned f_stretch:1;
    unsigned f_dynamic:1;
    unsigned f_rgb:1;
    unsigned f_r:1;
    unsigned f_g:1;
    unsigned f_b:1;
    unsigned f_whiteBorder:1;
    unsigned f_shear:1;
    unsigned f_rotate:1;
    unsigned f_mirror:1;
    unsigned f_crop:1;
    unsigned f_setPixels:1;
    unsigned f_keepPixels:1;

    int closing;
    int opening;
    int stretchT1;
    int stretchT2;
    int dynamicW;
    int dynamicH;
    int whiteBorder;
    int shear;
    int rotate;
    int mirror;
    RECT crop;
    int setPixels;
    int keepPixels;
};

struct ssocr {
    struct ssocr_options options;
    struct ssocr_commands commands;

    unsigned f_image:1;
    char image_name[FULL_BUF_LEN];

    char config_name[FULL_BUF_LEN];
};

struct comm_serial {
    unsigned f_baud:1;
    unsigned f_rts:1;
    unsigned f_echo:1;
    unsigned f_tout:1;
    unsigned f_recv_xon:1;
    unsigned f_trans_xon:1;

    int baud;
    int rts;
    int echo;
    int tout;
};

struct comm_tcp {
    unsigned f_port:1;

    int port;
};

struct comm_lan {
    unsigned f_ipaddr:1;
    unsigned f_subnet:1;
    unsigned f_gateway:1;
    unsigned f_name1:1;
    unsigned f_name2:1;
    unsigned f_host:1;

    struct in_addr ipaddr;
    struct in_addr subnet;
    struct in_addr gateway;
    struct in_addr name1;
    struct in_addr name2;
    char host[FULL_BUF_LEN];
};

struct communicate {
    struct comm_serial serial;
    struct comm_tcp tcp;
    struct comm_lan lan;
};

struct sysmgmt {
};

struct scpicam {
    struct sysmgmt sys;
    struct communicate comm;
    struct raspistill camera;
    struct ssocr ocr;
    char config_name[FULL_BUF_LEN];

    unsigned f_camera:1;
    unsigned f_ocr:1;
    unsigned f_running:1;
    unsigned f_operating:1;
    unsigned f_socket_restart:1;
};

#define WIDTH_MIN	0
#define WIDTH_MAX	65535
#define HEIGHT_MIN	0
#define HEIGHT_MAX	65535
#define QUALITY_MIN	0
#define QUALITY_MAX	100
#define TIMEOUT_MIN	5
#define TIMEOUT_MAX	65535
#define TIMELAPSE_MIN	1
#define TIMELAPSE_MAX	65535
#define DEMO_MIN	1
#define DEMO_MAX	65535
#define SHARPNESS_MIN	-100
#define SHARPNESS_MAX	100
#define CONTRAST_MIN	-100
#define CONTRAST_MAX	100
#define BRIGHTNESS_MIN	0
#define BRIGHTNESS_MAX	100
#define SATURATION_MIN	-100
#define SATURATION_MAX	100
#define ISO_MIN		100
#define ISO_MAX		800
#define EV_MIN		-10
#define EV_MAX		10
#define UV_MIN		0
#define UV_MAX		255

#define THRESHOLD_MIN	0
#define THRESHOLD_MAX	100
#define PIXEL_MIN	1
#define PIXEL_MAX	65535
#define NUMBER_MIN	1
#define NUMBER_MAX	65535
#define RATIO_MIN	1
#define RATIO_MAX	65535
#define STRETCH_MIN	0
#define STRETCH_MAX	255
#define DEGREE_MIN	0
#define DEGREE_MAX	360

#define YEAR_MIN	1900
#define YEAR_MAX	2100
#define MONTH_MIN	1
#define MONTH_MAX	12
#define DAY_MIN		1
#define DAY_MAX		31
#define HOUR_MIN	0
#define HOUR_MAX	23
#define MINUTE_MIN	0
#define MINUTE_MAX	59
#define SECOND_MIN	0
#define SECOND_MAX	59

#define PORT_MIN	1001
#define PORT_MAX	65535

#define IF_RANGE(value, min, max) \
    if ((value >= min) && (value <= max)) {

#define ELSEIF_RANGE \
    } else {

#define ENDIF_RANGE \
    }

#define CONFIG_STORE(o, e, f, v...) \
    if (o == e) \
    { \
	memset(command, 0, COMMAND_BUF_LEN); \
	sprintf(command, f, ##v); \
	len = strlen(command); \
	fwrite(command, 1, len, fp); \
    }

#define CONFIG_CONVERT(o, e, f, v...) \
    if (o == e) \
    { \
	memset(token, 0, 32); \
	sprintf(token, f, ##v); \
	len = strlen(token); \
	token[len] = '\0'; \
	ptr = strcat(ptr, token); \
    }
