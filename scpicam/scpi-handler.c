#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <resolv.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "scpi/scpi.h"
#include "scpi-def.h"
#include "scpi-utils.h"
#include "scpi-config.h"
#include "scpi-uart.h"
#include "scpi-tcp.h"

#define COMMAND_BUF_LEN	256
#define RESULT_BUF_LEN	256

const char * trigger_exposure[] = {
    "off",
    "auto",
    "night",
    "nightpreview",
    "backlight",
    "spotlight",
    "sports",
    "snow",
    "beach",
    "verylong",
    "fixedfps",
    "antishake",
    "fireworks",
};

const char * trigger_awb[] = {
    "off",
    "auto",
    "sun",
    "cloudshade",
    "tungsten",
    "fluorescent",
    "incandescent",
    "flash",
    "horizon",
};

const char * trigger_imxfx[] = {
    "none",
    "negative",
    "solarise",
    "whiteboard",
    "blackboard",
    "sketch",
    "denoise",
    "emboss",
    "oilpaint",
    "hatch",
    "gpen",
    "pastel",
    "watercolour",
    "film",
    "blur",
    "saturation",
    "colourswap",
    "washedout",
    "posterise",
    "colourpoint",
    "colourbalance",
    "cartoon",
};

const char * trigger_encoding[] = {
    "jpg",
    "bmp",
    "gif",
    "png",
};

const char * trigger_metering[] = {
    "average",
    "spot",
    "backlit",
    "matrix",
};

const char * trigger_rotation[] = {
    "0",
    "90",
    "180",
    "270",
};

const char * trigger_state[] = {
    "RUN",
    "STOP",
};

const char * trigger_format[] = {
    "jpg",
    "gif",
    "ppm",
    "pgm",
    "xpm",
    "png",
    "tiff",
    "eim",
};

const char * trigger_color[] = {
    "black",
    "white",
};

const char * trigger_luminance[] = {
    "rec601",
    "rec709",
    "linear",
    "minimum",
    "maximum",
    "reg",
    "green",
    "blue",
};

const char * trigger_mirror[] = {
    "horiz",
    "vert",
};

const char * trigger_baud[] = {
    "300",
    "1200",
    "2400",
    "4800",
    "9600",
    "19200",
    "38400",
};

const char * trigger_rts[] = {
    "ON",
    "OFF",
    "IBFull",
    "RFR",
};

const char * trigger_echo[] = {
    "OFF",
    "ON",
    "0",
    "1",
};

const char * trigger_pace[] = {
    "NONE",
    "XON",
};

struct scpicam global_conf;
bool f_loadable = false;
static char command[COMMAND_BUF_LEN];
static char result[COMMAND_BUF_LEN];

struct scpicam * GetConfiguration(void) {
    return &global_conf;
}

scpi_result_t LoadConfiguration(void) {
    FILE * fp = NULL;

    if (!f_loadable)
	return SCPI_RES_ERR;

    if (global_conf.f_camera && global_conf.f_ocr)
    {
	if (strlen(global_conf.config_name) <= 0)
	    return SCPI_RES_ERR;

	fp = fopen(global_conf.config_name, "r");
	if(fp == NULL)
	    return SCPI_RES_ERR;
    } else if (global_conf.f_camera) {
	if (strlen(global_conf.camera.config_name) <= 0)
	    return SCPI_RES_ERR;

	fp = fopen(global_conf.camera.config_name, "r");
	if(fp == NULL)
	    return SCPI_RES_ERR;
    } else {
	if (strlen(global_conf.ocr.config_name) <= 0)
	    return SCPI_RES_ERR;

	fp = fopen(global_conf.ocr.config_name, "r");
	if(fp == NULL)
	    return SCPI_RES_ERR;

    }

    memset(command, 0, COMMAND_BUF_LEN);
    while(fgets(command, COMMAND_BUF_LEN, fp) != NULL)
    {
	SCPI_Input(&scpi_context, command, strlen(command));
	memset(command, 0, COMMAND_BUF_LEN);
    }

    fclose(fp);

    f_loadable = false;
    if (global_conf.f_camera && global_conf.f_ocr)
    {
	memset(global_conf.config_name, 0, FULL_BUF_LEN);
	global_conf.f_camera = 0;
	global_conf.f_ocr = 0;
    } else if (global_conf.f_camera) {
	memset(global_conf.camera.config_name, 0, FULL_BUF_LEN);
	global_conf.f_camera = 0;
    } else {
	memset(global_conf.ocr.config_name, 0, FULL_BUF_LEN);
	global_conf.f_ocr = 0;
    }

    return SCPI_RES_OK;
}

scpi_result_t SCPI_ExecuteCommand(const char * string, bool f_always) {

    DPRINT("Execute: %s\n", string);

#if !defined (SCPICAM_DEBUG)
    if (global_conf.f_operating || f_always)
#endif
	system(string);

    return SCPI_RES_OK;
}

static scpi_result_t ocr_Result() {
    FILE * fp = NULL;

    memset(result, 0, sizeof(COMMAND_BUF_LEN));
    fp = fopen("ocr_result", "r");
    if(fp == NULL){
        return SCPI_RES_ERR;
    }

    while(fgets(result, COMMAND_BUF_LEN, fp) != NULL)
    {
	SCPI_ResultString(&scpi_context, result);
	memset(result, 0, COMMAND_BUF_LEN);
    }

    return SCPI_RES_OK;
}

static scpi_result_t global_LoadConfiguration(FILE * fp, const char * name, int len) {
    f_loadable = true;
    memset(&global_conf, 0, sizeof(struct scpicam));
    global_conf.f_camera = 1;
    global_conf.f_ocr = 1;
    memcpy(global_conf.config_name, name, len);

    return SCPI_RES_OK;
}

static scpi_result_t camera_ExecuteConfiguration(char * string) {
    int len = 0;
    char token[32];
    char * ptr = string;

    sprintf(ptr, "raspistill");
    ptr += 10;

    CONFIG_CONVERT(global_conf.camera.options.f_width, 1, " -w %d", \
	    global_conf.camera.options.width);
    CONFIG_CONVERT(global_conf.camera.options.f_height, 1, " -h %d", \
	    global_conf.camera.options.height);
    CONFIG_CONVERT(global_conf.camera.options.f_quality, 1, " -q %d", \
	    global_conf.camera.options.quality);
    CONFIG_CONVERT(global_conf.camera.options.f_raw, 1, " -r");
    CONFIG_CONVERT(global_conf.camera.options.f_output, 1, " -o %s", \
	    global_conf.camera.options.output);
    CONFIG_CONVERT(global_conf.camera.options.f_verbose, 1, " -v");
    CONFIG_CONVERT(global_conf.camera.options.f_timeout, 1, " -t %d", \
	    global_conf.camera.options.timeout);
    CONFIG_CONVERT(global_conf.camera.options.f_timelapse, 1, " -tl %d", \
	    global_conf.camera.options.timelapse);
    CONFIG_CONVERT(global_conf.camera.options.f_thumb, 1, " -th %d:%d:%d", \
	    global_conf.camera.options.thumb.x, global_conf.camera.options.thumb.y, \
	    global_conf.camera.options.thumb.quality);
    CONFIG_CONVERT(global_conf.camera.options.f_demo, 1, " -d %d", \
	    global_conf.camera.options.demo);
    CONFIG_CONVERT(global_conf.camera.options.f_encoding, 1, " -e %s", \
	    trigger_encoding[global_conf.camera.options.encoding]);

    CONFIG_CONVERT(global_conf.camera.view.f_preview, 1, " -p %d,%d,%d,%d", \
	    global_conf.camera.view.preview.x, global_conf.camera.view.preview.y, \
	    global_conf.camera.view.preview.w, global_conf.camera.view.preview.h);
    CONFIG_CONVERT(global_conf.camera.view.f_preview, 0, " -n");
    CONFIG_CONVERT(global_conf.camera.view.f_full, 1, " -f");

    CONFIG_CONVERT(global_conf.camera.image.f_sharpness, 1, " -sh %d", \
	    global_conf.camera.image.sharpness);
    CONFIG_CONVERT(global_conf.camera.image.f_contrast, 1, " -co %d", \
	    global_conf.camera.image.contrast);
    CONFIG_CONVERT(global_conf.camera.image.f_brightness, 1, " -br %d", \
	    global_conf.camera.image.brightness);
    CONFIG_CONVERT(global_conf.camera.image.f_saturation, 1, " -sa %d", \
	    global_conf.camera.image.saturation);
    CONFIG_CONVERT(global_conf.camera.image.f_iso, 1, " -ISO %d", \
	    global_conf.camera.image.iso);
    CONFIG_CONVERT(global_conf.camera.image.f_ev, 1, " -ev %d", \
	    global_conf.camera.image.ev);
    CONFIG_CONVERT(global_conf.camera.image.f_exposure, 1, " -ex %s", \
	    trigger_exposure[global_conf.camera.image.exposure]);
    CONFIG_CONVERT(global_conf.camera.image.f_awb, 1, " -awb %s", \
	    trigger_awb[global_conf.camera.image.awb]);
    CONFIG_CONVERT(global_conf.camera.image.f_imxfx, 1, " -ifx %s", \
	    trigger_imxfx[global_conf.camera.image.imxfx]);
    CONFIG_CONVERT(global_conf.camera.image.f_colfx, 1, " -cfx %d:%d", \
	    global_conf.camera.image.colfx.u, global_conf.camera.image.colfx.v);
    CONFIG_CONVERT(global_conf.camera.image.f_metering, 1, " -mm %s", \
	    trigger_metering[global_conf.camera.image.metering]);
    CONFIG_CONVERT(global_conf.camera.image.f_rotation, 1, " -rot %s", \
	    trigger_rotation[global_conf.camera.image.rotation]);
    CONFIG_CONVERT(global_conf.camera.image.f_vstab, 1, " -vs");
    CONFIG_CONVERT(global_conf.camera.image.f_hflip, 1, " -hf");
    CONFIG_CONVERT(global_conf.camera.image.f_vflip, 1, " -vf");

    return SCPI_RES_OK;
}

static scpi_result_t camera_StoreConfiguration(FILE * fp) {
    int len;

    /* store options */
    CONFIG_STORE(global_conf.camera.options.f_width, 1, "%s %d\n", \
	    CAM_CONF_OPT_WID, global_conf.camera.options.width);
    CONFIG_STORE(global_conf.camera.options.f_height, 1, "%s %d\n", \
	    CAM_CONF_OPT_HEI, global_conf.camera.options.height);
    CONFIG_STORE(global_conf.camera.options.f_quality, 1, "%s %d\n", \
	    CAM_CONF_OPT_QUA, global_conf.camera.options.quality);
    CONFIG_STORE(global_conf.camera.options.f_raw, 1, "%s\n", \
	    CAM_CONF_OPT_RAW);
    CONFIG_STORE(global_conf.camera.options.f_output, 1, "%s %s\n", \
	    CAM_CONF_OPT_OUT, global_conf.camera.options.output);
    CONFIG_STORE(global_conf.camera.options.f_verbose, 1, "%s\n", \
	    CAM_CONF_OPT_VER);
    CONFIG_STORE(global_conf.camera.options.f_timeout, 1, "%s %d\n", \
	    CAM_CONF_OPT_TIM, global_conf.camera.options.timeout);
    CONFIG_STORE(global_conf.camera.options.f_thumb, 1, "%s %d,%d,%d\n", \
	    CAM_CONF_OPT_THU, global_conf.camera.options.thumb.x, \
	    global_conf.camera.options.thumb.y, global_conf.camera.options.thumb.quality);
    CONFIG_STORE(global_conf.camera.options.f_demo, 1, "%s\n", \
	    CAM_CONF_OPT_DEM);
    CONFIG_STORE(global_conf.camera.options.f_timelapse, 1, "%s %d\n", \
	    CAM_CONF_OPT_TIME, global_conf.camera.options.timelapse);
    CONFIG_STORE(global_conf.camera.options.f_demo, 1, "%s %d\n", \
	    CAM_CONF_OPT_DEM, global_conf.camera.options.demo);
    CONFIG_STORE(global_conf.camera.options.f_encoding, 1, "%s %s\n", \
	    CAM_CONF_OPT_ENC, trigger_encoding[global_conf.camera.options.encoding]);

    /* store view */
    CONFIG_STORE(global_conf.camera.view.f_preview, 1, "%s %d,%d,%d,%d\n", \
	    CAM_CONF_VIE_PRE, global_conf.camera.view.preview.x, global_conf.camera.view.preview.y, \
	    global_conf.camera.view.preview.w, global_conf.camera.view.preview.h);
    CONFIG_STORE(global_conf.camera.view.f_full, 1, "%s\n", \
	    CAM_CONF_VIE_FUL);

    /* store image */
    CONFIG_STORE(global_conf.camera.image.f_sharpness, 1, "%s %d\n", \
	    CAM_CONF_IMA_SHA, global_conf.camera.image.sharpness);
    CONFIG_STORE(global_conf.camera.image.f_contrast, 1, "%s %d\n", \
	    CAM_CONF_IMA_CON, global_conf.camera.image.contrast);
    CONFIG_STORE(global_conf.camera.image.f_brightness, 1, "%s %d\n", \
	    CAM_CONF_IMA_BRI, global_conf.camera.image.brightness);
    CONFIG_STORE(global_conf.camera.image.f_saturation, 1, "%s %d\n", \
	    CAM_CONF_IMA_SAT, global_conf.camera.image.saturation);
    CONFIG_STORE(global_conf.camera.image.f_iso, 1, "%s %d\n", \
	    CAM_CONF_IMA_ISO, global_conf.camera.image.iso);
    CONFIG_STORE(global_conf.camera.image.f_ev, 1, "%s %d\n", \
	    CAM_CONF_IMA_EV, global_conf.camera.image.ev);
    CONFIG_STORE(global_conf.camera.image.f_exposure, 1, "%s %s\n", \
	    CAM_CONF_IMA_EXP, trigger_exposure[global_conf.camera.image.exposure]);
    CONFIG_STORE(global_conf.camera.image.f_awb, 1, "%s %s\n", \
	    CAM_CONF_IMA_AWB, trigger_awb[global_conf.camera.image.awb]);
    CONFIG_STORE(global_conf.camera.image.f_imxfx, 1, "%s %s\n", \
	    CAM_CONF_IMA_IMX, trigger_imxfx[global_conf.camera.image.imxfx]);
    CONFIG_STORE(global_conf.camera.image.f_colfx, 1, "%s %d,%d\n", \
	    CAM_CONF_IMA_COL, global_conf.camera.image.colfx.u, global_conf.camera.image.colfx.v);
    CONFIG_STORE(global_conf.camera.image.f_metering, 1, "%s %s\n", \
	    CAM_CONF_IMA_MET, trigger_metering[global_conf.camera.image.metering]);
    CONFIG_STORE(global_conf.camera.image.f_rotation, 1, "%s %s\n", \
	    CAM_CONF_IMA_ROT, trigger_rotation[global_conf.camera.image.rotation]);
    CONFIG_STORE(global_conf.camera.image.f_vstab, 1, "%s\n", \
	    CAM_CONF_IMA_VST);
    CONFIG_STORE(global_conf.camera.image.f_hflip, 1, "%s\n", \
	    CAM_CONF_IMA_HFL);
    CONFIG_STORE(global_conf.camera.image.f_vflip, 1, "%s\n", \
	    CAM_CONF_IMA_VFL);

    return SCPI_RES_OK;
}

static scpi_result_t camera_LoadConfiguration(FILE * fp, const char * name, int len) {
    f_loadable = true;
    memset(&global_conf.camera, 0, sizeof(struct raspistill));
    global_conf.f_camera = 1;
    memcpy(global_conf.camera.config_name, name, len);

    return SCPI_RES_OK;
}

static scpi_result_t ocr_ExecuteConfiguration(char * string) {
    int len = 0;
    char token[32];
    char * ptr = string;

    if (!global_conf.ocr.f_image)
	return SCPI_RES_ERR;

    sprintf(ptr, "ssocr");
    ptr += 5;

    CONFIG_CONVERT(global_conf.ocr.options.f_verbose, 1, " -v");
    CONFIG_CONVERT(global_conf.ocr.options.f_threshold, 1, " -t %d", \
	    global_conf.ocr.options.threshold);
    CONFIG_CONVERT(global_conf.ocr.options.f_absolute, 1, " -a");
    CONFIG_CONVERT(global_conf.ocr.options.f_iter, 1, " -T");
    CONFIG_CONVERT(global_conf.ocr.options.f_numberPixels, 1, " -n %d", \
	    global_conf.ocr.options.numberPixels);
    CONFIG_CONVERT(global_conf.ocr.options.f_ignorePixels, 1, " -i %d", \
	    global_conf.ocr.options.ignorePixels);
    CONFIG_CONVERT(global_conf.ocr.options.f_numberDigits, 1, " -d %d", \
	    global_conf.ocr.options.numberDigits);
    CONFIG_CONVERT(global_conf.ocr.options.f_oneRatio, 1, " -r %d", \
	    global_conf.ocr.options.oneRatio);
    CONFIG_CONVERT(global_conf.ocr.options.f_minusRatio, 1, " -m %d", \
	    global_conf.ocr.options.minusRatio);
    CONFIG_CONVERT(global_conf.ocr.options.f_outputImage, 1, " -o %s", \
	    global_conf.ocr.options.outputImage);
    CONFIG_CONVERT(global_conf.ocr.options.f_outputFormat, 1, " -O %s", \
	    trigger_format[global_conf.ocr.options.outputFormat]);
    CONFIG_CONVERT(global_conf.ocr.options.f_processOnly, 1, " -p");
    CONFIG_CONVERT(global_conf.ocr.options.f_debugImage, 1, " -D %s", \
	    global_conf.ocr.options.debugImage);
    CONFIG_CONVERT(global_conf.ocr.options.f_debugOutput, 1, " -P");
    CONFIG_CONVERT(global_conf.ocr.options.f_foreground, 1, " -f %s", \
	    trigger_color[global_conf.ocr.options.foreground]);
    CONFIG_CONVERT(global_conf.ocr.options.f_background, 1, " -b %s", \
	    trigger_color[global_conf.ocr.options.background]);
    CONFIG_CONVERT(global_conf.ocr.options.f_printInfo, 1, " -I");
    CONFIG_CONVERT(global_conf.ocr.options.f_adjustGray, 1, " -g");
    CONFIG_CONVERT(global_conf.ocr.options.f_luminance, 1, " -l %s", \
	    trigger_luminance[global_conf.ocr.options.luminance]);

    CONFIG_CONVERT(global_conf.ocr.commands.f_dilation, 1, " dilation");
    CONFIG_CONVERT(global_conf.ocr.commands.f_erosion, 1, " erosion");
    CONFIG_CONVERT(global_conf.ocr.commands.f_closing, 1, " closing %d", \
	    global_conf.ocr.commands.closing);
    CONFIG_CONVERT(global_conf.ocr.commands.f_opening, 1, " opening %d", \
	    global_conf.ocr.commands.opening);
    CONFIG_CONVERT(global_conf.ocr.commands.f_removeIsolated, 1, " remove_isolated");
    CONFIG_CONVERT(global_conf.ocr.commands.f_makeMono, 1, " make_mono");
    CONFIG_CONVERT(global_conf.ocr.commands.f_grayscale, 1, " grayscale");
    CONFIG_CONVERT(global_conf.ocr.commands.f_invert, 1, " invert");
    CONFIG_CONVERT(global_conf.ocr.commands.f_stretch, 1, " gray_stretch %d %d", \
	    global_conf.ocr.commands.stretchT1, global_conf.ocr.commands.stretchT2);
    CONFIG_CONVERT(global_conf.ocr.commands.f_dynamic, 1, " dynamic_threshold %d %d", \
	    global_conf.ocr.commands.dynamicW, global_conf.ocr.commands.dynamicH);
    CONFIG_CONVERT(global_conf.ocr.commands.f_rgb, 1, " rgb_threshold");
    CONFIG_CONVERT(global_conf.ocr.commands.f_r, 1, " r_threshold");
    CONFIG_CONVERT(global_conf.ocr.commands.f_g, 1, " g_threshold");
    CONFIG_CONVERT(global_conf.ocr.commands.f_b, 1, " b_threshold");
    CONFIG_CONVERT(global_conf.ocr.commands.f_whiteBorder, 1, " white_border %d", \
	    global_conf.ocr.commands.whiteBorder);
    CONFIG_CONVERT(global_conf.ocr.commands.f_shear, 1, " shear %d", \
	    global_conf.ocr.commands.shear);
    CONFIG_CONVERT(global_conf.ocr.commands.f_rotate, 1, " rotate %d", \
	    global_conf.ocr.commands.rotate);
    CONFIG_CONVERT(global_conf.ocr.commands.f_mirror, 1, " mirror %s", \
	    trigger_mirror[global_conf.ocr.commands.mirror]);
    CONFIG_CONVERT(global_conf.ocr.commands.f_crop, 1, " crop %d %d %d %d", \
	    global_conf.ocr.commands.crop.x, global_conf.ocr.commands.crop.y, \
	    global_conf.ocr.commands.crop.w, global_conf.ocr.commands.crop.h);
    CONFIG_CONVERT(global_conf.ocr.commands.f_setPixels, 1, " set_pixels_filter %d", \
	    global_conf.ocr.commands.setPixels);
    CONFIG_CONVERT(global_conf.ocr.commands.f_keepPixels, 1, " keep_pixels_filter %d", \
	    global_conf.ocr.commands.keepPixels);

    CONFIG_CONVERT(global_conf.ocr.f_image, 1, " %s", \
	    global_conf.ocr.image_name);

    strcat(ptr, ">ocr_result");

    return SCPI_RES_OK;
}

static scpi_result_t ocr_StoreConfiguration(FILE * fp) {
    int len;

    CONFIG_STORE(global_conf.ocr.f_image, 1, "%s %s\n", \
	    OCR_CONF_IMAGE, global_conf.ocr.image_name);
    /* store options */
    CONFIG_STORE(global_conf.ocr.options.f_verbose, 1, "%s\n", \
	    OCR_CONF_OPT_VERB);
    CONFIG_STORE(global_conf.ocr.options.f_threshold, 1, "%s %d\n", \
	    OCR_CONF_OPT_THR, global_conf.ocr.options.threshold);
    CONFIG_STORE(global_conf.ocr.options.f_absolute, 1, "%s\n", \
	    OCR_CONF_OPT_ABST);
    CONFIG_STORE(global_conf.ocr.options.f_iter, 1, "%s\n", \
	    OCR_CONF_OPT_ITRT);
    CONFIG_STORE(global_conf.ocr.options.f_numberPixels, 1, "%s %d\n", \
	    OCR_CONF_OPT_NUMP, global_conf.ocr.options.numberPixels);
    CONFIG_STORE(global_conf.ocr.options.f_ignorePixels, 1, "%s %d\n", \
	    OCR_CONF_OPT_IGNP, global_conf.ocr.options.ignorePixels);
    CONFIG_STORE(global_conf.ocr.options.f_numberDigits, 1, "%s %d\n", \
	    OCR_CONF_OPT_DIGN, global_conf.ocr.options.numberDigits);
    CONFIG_STORE(global_conf.ocr.options.f_oneRatio, 1, "%s %d\n", \
	    OCR_CONF_OPT_ONER, global_conf.ocr.options.oneRatio);
    CONFIG_STORE(global_conf.ocr.options.f_minusRatio, 1, "%s %d\n", \
	    OCR_CONF_OPT_MINR, global_conf.ocr.options.minusRatio);
    CONFIG_STORE(global_conf.ocr.options.f_outputImage, 1, "%s %s\n", \
	    OCR_CONF_OPT_OUTI, global_conf.ocr.options.outputImage);
    CONFIG_STORE(global_conf.ocr.options.f_outputFormat, 1, "%s %s\n", \
	    OCR_CONF_OPT_OUTF, trigger_format[global_conf.ocr.options.outputFormat]);
    CONFIG_STORE(global_conf.ocr.options.f_processOnly, 1, "%s\n", \
	    OCR_CONF_OPT_PRO);
    CONFIG_STORE(global_conf.ocr.options.f_debugImage, 1, "%s %s\n", \
	    OCR_CONF_OPT_DBGI, global_conf.ocr.options.debugImage);
    CONFIG_STORE(global_conf.ocr.options.f_debugOutput, 1, "%s\n", \
	    OCR_CONF_OPT_DBGO);
    CONFIG_STORE(global_conf.ocr.options.f_foreground, 1, "%s %s\n", \
	    OCR_CONF_OPT_FORE, trigger_color[global_conf.ocr.options.foreground]);
    CONFIG_STORE(global_conf.ocr.options.f_background, 1, "%s %s\n", \
	    OCR_CONF_OPT_BACK, trigger_color[global_conf.ocr.options.background]);
    CONFIG_STORE(global_conf.ocr.options.f_printInfo, 1, "%s\n", \
	    OCR_CONF_OPT_PRNI);
    CONFIG_STORE(global_conf.ocr.options.f_adjustGray, 1, "%s\n", \
	    OCR_CONF_OPT_ADJG);
    CONFIG_STORE(global_conf.ocr.options.f_luminance, 1, "%s %s\n", \
	    OCR_CONF_OPT_LUMI, trigger_luminance[global_conf.ocr.options.luminance]);

    /* store commands */
    CONFIG_STORE(global_conf.ocr.commands.f_dilation, 1, "%s\n", \
	    OCR_CONF_COM_DILA);
    CONFIG_STORE(global_conf.ocr.commands.f_erosion, 1, "%s\n", \
	    OCR_CONF_COM_EROS);
    CONFIG_STORE(global_conf.ocr.commands.f_closing, 1, "%s %d\n", \
	    OCR_CONF_COM_CLOS, global_conf.ocr.commands.closing);
    CONFIG_STORE(global_conf.ocr.commands.f_opening, 1, "%s %d\n", \
	    OCR_CONF_COM_OPEN, global_conf.ocr.commands.opening);
    CONFIG_STORE(global_conf.ocr.commands.f_removeIsolated, 1, "%s\n", \
	    OCR_CONF_COM_RMVI);
    CONFIG_STORE(global_conf.ocr.commands.f_makeMono, 1, "%s\n", \
	    OCR_CONF_COM_MONO);
    CONFIG_STORE(global_conf.ocr.commands.f_grayscale, 1, "%s\n", \
	    OCR_CONF_COM_GRAY);
    CONFIG_STORE(global_conf.ocr.commands.f_invert, 1, "%s\n", \
	    OCR_CONF_COM_INV);
    CONFIG_STORE(global_conf.ocr.commands.f_stretch, 1, "%s %d,%d\n", \
	    OCR_CONF_COM_STRE, global_conf.ocr.commands.stretchT1, \
	    global_conf.ocr.commands.stretchT2);
    CONFIG_STORE(global_conf.ocr.commands.f_dynamic, 1, "%s %d,%d\n", \
	    OCR_CONF_COM_DYN, global_conf.ocr.commands.dynamicW, \
	    global_conf.ocr.commands.dynamicH);
    CONFIG_STORE(global_conf.ocr.commands.f_rgb, 1, "%s\n", \
	    OCR_CONF_COM_RGB);
    CONFIG_STORE(global_conf.ocr.commands.f_r, 1, "%s\n", \
	    OCR_CONF_COM_RTH);
    CONFIG_STORE(global_conf.ocr.commands.f_g, 1, "%s\n", \
	    OCR_CONF_COM_GTH);
    CONFIG_STORE(global_conf.ocr.commands.f_b, 1, "%s\n", \
	    OCR_CONF_COM_BTH);
    CONFIG_STORE(global_conf.ocr.commands.f_whiteBorder, 1, "%s %d\n", \
	    OCR_CONF_COM_WHI, global_conf.ocr.commands.whiteBorder);
    CONFIG_STORE(global_conf.ocr.commands.f_shear, 1, "%s %d\n", \
	    OCR_CONF_COM_SHE, global_conf.ocr.commands.shear);
    CONFIG_STORE(global_conf.ocr.commands.f_rotate, 1, "%s %d\n", \
	    OCR_CONF_COM_ROT, global_conf.ocr.commands.rotate);
    CONFIG_STORE(global_conf.ocr.commands.f_mirror, 1, "%s %s\n", \
	    OCR_CONF_COM_MIR, trigger_mirror[global_conf.ocr.commands.mirror]);
    CONFIG_STORE(global_conf.ocr.commands.f_crop, 1, "%s %d,%d,%d,%d\n", \
	    OCR_CONF_COM_CROP, global_conf.ocr.commands.crop.x, \
	    global_conf.ocr.commands.crop.y, global_conf.ocr.commands.crop.w, \
	    global_conf.ocr.commands.crop.h);
    CONFIG_STORE(global_conf.ocr.commands.f_setPixels, 1, "%s %d\n", \
	    OCR_CONF_COM_SETP, global_conf.ocr.commands.setPixels);
    CONFIG_STORE(global_conf.ocr.commands.f_keepPixels, 1, "%s %d\n", \
	    OCR_CONF_COM_KEEP, global_conf.ocr.commands.keepPixels);

    return SCPI_RES_OK;
}

static scpi_result_t ocr_LoadConfiguration(FILE * fp, const char * name, int len) {
    f_loadable = true;
    memset(&global_conf.ocr, 0, sizeof(struct ssocr));
    global_conf.f_ocr = 1;
    memcpy(global_conf.ocr.config_name, name, len);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_RaspberryPreset(scpi_t * context) {
    int ret;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "shutdown -r now");
    ret = system(command);
    if (ret != 0)
	SCPI_ResultInt(context, EX_SOFTWARE);
    else
	SCPI_ResultInt(context, EX_OK);

    return ret == 0 ? SCPI_RES_OK : SCPI_RES_ERR;
}

scpi_result_t SCPI_RaspberryVersionQ(scpi_t * context) {
    char ver[16];

    SCPICAM_VERSION(ver);
    SCPI_ResultString(context, ver);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_RaspberryConfigInitialize(scpi_t * context) {
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    memset(command, 0, COMMAND_BUF_LEN);

    len = get_absolutePath("init", 4, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "r");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    ret = global_LoadConfiguration(fp, absolute_path, len);
    if (ret != SCPI_RES_OK)
    {
	fclose(fp);
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fclose(fp);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_RaspberryConfigDelete(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "r");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    ret = remove(absolute_path);
    if (ret != 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_RaspberryConfigLoad(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "r");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    ret = global_LoadConfiguration(fp, absolute_path, len);
    if (ret != SCPI_RES_OK)
    {
	fclose(fp);
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_RaspberryConfigStore(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "w+");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    ret = camera_StoreConfiguration(fp);
    if (ret != SCPI_RES_OK)
    {
	fclose(fp);
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_RaspberryProgramState(scpi_t * context) {
    return SCPI_RES_OK;
}

scpi_result_t SCPI_RaspberryCameraState(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_OK;
    int param;
    char string[COMMAND_BUF_LEN];

    memset(string, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_state, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (global_conf.f_operating) {
	ret = camera_ExecuteConfiguration(string);
	if (ret == SCPI_RES_OK) {
	    ret = SCPI_ExecuteCommand(string, 0);
	}
    } else {
	global_conf.f_running = param == 0 ? 1 : 0;
    }

    if (ret == SCPI_RES_OK)
	SCPI_ResultInt(context, EX_OK);
    else
	SCPI_ResultInt(context, EX_SOFTWARE);

    return ret;
}

scpi_result_t SCPI_RaspberryOcrState(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_OK;
    int param;
    char string[COMMAND_BUF_LEN];

    memset(string, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_state, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (global_conf.f_operating) {
	ret = ocr_ExecuteConfiguration(string);
	if (ret == SCPI_RES_OK) {
	    SCPI_ExecuteCommand(string, 0);
	    ocr_Result();
	}
    } else {
	global_conf.f_running = param == 0 ? 1 : 0;
    }

    if (ret == SCPI_RES_OK)
	SCPI_ResultInt(context, EX_OK);
    else
	SCPI_ResultInt(context, EX_SOFTWARE);

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsWidth(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, WIDTH_MIN, WIDTH_MAX)
	sprintf(command, "raspistill -w %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.options.f_width = 1;
	    global_conf.camera.options.width = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsWidthQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tWidth=%d\r\n", global_conf.camera.options.width);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsHeight(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, HEIGHT_MIN, HEIGHT_MAX)
	sprintf(command, "raspistill -h %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.options.f_height = 1;
	    global_conf.camera.options.height = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsHeightQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tHeight=%d\r\n", global_conf.camera.options.height);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsQuality(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, QUALITY_MIN, QUALITY_MAX)
	sprintf(command, "raspistill -q %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.options.f_quality = 1;
	    global_conf.camera.options.quality = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsQualityQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tQuality=%d\r\n", global_conf.camera.options.quality);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsRaw(scpi_t * context){
    scpi_result_t ret = SCPI_RES_ERR;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "raspistill -r");
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.options.f_raw = 1;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsOutput(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    const char * param1;
    size_t len;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    sprintf(command, "raspistill -o %s", param1);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.options.f_output = 1;
	memset(global_conf.camera.options.output, 0, (len + 1));
	memcpy(global_conf.camera.options.output, param1, len);
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsOutputQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tOutput=%s\r\n", global_conf.camera.options.output);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsVerbose(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "raspistill -v");
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.options.f_verbose = 1;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsTimeout(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, TIMEOUT_MIN, TIMEOUT_MAX)
	sprintf(command, "raspistill -t %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.options.f_timeout = 1;
	    global_conf.camera.options.timeout = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsTimeoutQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tTimeout=%d\r\n", global_conf.camera.options.timeout);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsThumb(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;
    int param2;
    int param3;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    if (!SCPI_ParamInt(context, &param3, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param2, HEIGHT_MIN, HEIGHT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param3, QUALITY_MIN, QUALITY_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    sprintf(command, "raspistill -th %d:%d:%d", param1, param2, param3);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.options.f_thumb = 1;
	global_conf.camera.options.thumb.x = param1;
	global_conf.camera.options.thumb.y = param2;
	global_conf.camera.options.thumb.quality = param3;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsThumbQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tThumb x=%d y=%d quality=%d\r\n", global_conf.camera.options.thumb.x, \
	    global_conf.camera.options.thumb.x, \
	    global_conf.camera.options.thumb.quality);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsDemo(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, DEMO_MIN, DEMO_MAX)
	sprintf(command, "raspistill -d %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.options.f_demo = 1;
	    global_conf.camera.options.demo = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsDemoQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tDemo=%d\r\n", global_conf.camera.options.demo);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsEncoding(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_encoding, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    sprintf(command, "raspistill -e %s", trigger_encoding[param]);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.options.f_encoding = 1;
	global_conf.camera.options.encoding = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsEncodingQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tEncoding=%s\r\n", trigger_encoding[global_conf.camera.options.encoding]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsExif(scpi_t * context) {

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureOptionsTimelapse(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, TIMELAPSE_MIN, TIMELAPSE_MAX)
	sprintf(command, "raspistill -tl %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.options.f_timelapse = 1;
	    global_conf.camera.options.timelapse = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureOptionsTimelapseQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tTimelapse=%d\r\n", global_conf.camera.options.timelapse);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureViewPreview(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;
    int param2;
    int param3;
    int param4;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    }

    if (!SCPI_ParamInt(context, &param3, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    }

    if (!SCPI_ParamInt(context, &param4, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param2, HEIGHT_MIN, HEIGHT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param3, WIDTH_MIN, WIDTH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param4, HEIGHT_MIN, HEIGHT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    sprintf(command, "raspistill -p %d,%d,%d,%d", param1, param2, param3, param4);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.view.f_preview = 1;
	global_conf.camera.view.preview.x = param1;
	global_conf.camera.view.preview.y = param2;
	global_conf.camera.view.preview.w = param3;
	global_conf.camera.view.preview.h = param4;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureViewFullscreen(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "raspistill -f");
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.view.f_full = 1;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureViewNopreview(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "raspistill -n");
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.view.f_preview = 0;
	global_conf.camera.view.preview.x = 0;
	global_conf.camera.view.preview.y = 0;
	global_conf.camera.view.preview.w = 0;
	global_conf.camera.view.preview.h = 0;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageSharpness(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, SHARPNESS_MIN, SHARPNESS_MAX)
	sprintf(command, "raspistill -sh %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.image.f_sharpness = 1;
	    global_conf.camera.image.sharpness = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageContrast(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, CONTRAST_MIN, CONTRAST_MAX)
	sprintf(command, "raspistill -co %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.image.f_contrast = 1;
	    global_conf.camera.image.contrast = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageBrightness(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, BRIGHTNESS_MIN, BRIGHTNESS_MAX)
	sprintf(command, "raspistill -br %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.image.f_brightness = 1;
	    global_conf.camera.image.brightness = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageSaturation(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, SATURATION_MIN, SATURATION_MAX)
	sprintf(command, "raspistill -sa %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.image.f_saturation = 1;
	    global_conf.camera.image.saturation = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageIso(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, ISO_MIN, ISO_MAX)
	sprintf(command, "raspistill -ISO %d", param1);
        ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.image.f_iso = 1;
	    global_conf.camera.image.iso = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageEv(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, EV_MIN, EV_MAX)
	sprintf(command, "raspistill -ev %d", param1);
	ret = SCPI_ExecuteCommand(command, 0);
	if (ret == SCPI_RES_OK) {
	    global_conf.camera.image.f_ev = 1;
	    global_conf.camera.image.ev = param1;
	    SCPI_ResultInt(context, EX_OK);
	} else {
	    SCPI_ResultInt(context, EX_SOFTWARE);
	}
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageExposure(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_exposure, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    sprintf(command, "raspistill -ex %s", trigger_exposure[param]);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_exposure = 1;
	global_conf.camera.image.exposure = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageAwb(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_awb, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    sprintf(command, "raspistill -awb %s", trigger_awb[param]);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_awb = 1;
	global_conf.camera.image.awb = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageImxfx(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_imxfx, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    sprintf(command, "raspistill -ifx %s", trigger_imxfx[param]);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_imxfx = 1;
	global_conf.camera.image.imxfx = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageColfx(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;
    int param2;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE(param1, UV_MIN, UV_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE(param2, UV_MIN, UV_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    sprintf(command, "raspistill -cfx %d:%d", param1, param2);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_colfx = 1;
	global_conf.camera.image.colfx.u = param1;
	global_conf.camera.image.colfx.v = param2;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageMetering(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_metering, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    sprintf(command, "raspistill -mm %s", trigger_metering[param]);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_metering = 1;
	global_conf.camera.image.metering = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageRotation(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamChoice(context, trigger_rotation, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    sprintf(command, "raspistill -rot %s", trigger_rotation[param]);
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_rotation = 1;
	global_conf.camera.image.rotation = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageSharpnessQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tSharpness=%d\r\n", global_conf.camera.image.sharpness);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageContrastQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tContrast=%d\r\n", global_conf.camera.image.contrast);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageBrightnessQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tBrightness=%d\r\n", global_conf.camera.image.brightness);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageSaturationQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tSaturation=%d\r\n", global_conf.camera.image.saturation);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageIsoQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tISO=%d\r\n", global_conf.camera.image.iso);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageEvQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tEV=%d\r\n", global_conf.camera.image.iso);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageExposureQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tExposure=%s\r\n", trigger_exposure[global_conf.camera.image.exposure]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageAwbQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tAwb=%s\r\n", trigger_awb[global_conf.camera.image.awb]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageImxfxQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tImxfx=%s\r\n", trigger_imxfx[global_conf.camera.image.imxfx]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageColfxQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tcolfx u=%d v=%d\r\n", global_conf.camera.image.colfx.u, global_conf.camera.image.colfx.v);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageMeteringQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tMetering=%s\r\n", trigger_metering[global_conf.camera.image.metering]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageRotationQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tRotation=%s\r\n", trigger_rotation[global_conf.camera.image.rotation]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraConfigureImageVstab(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "raspistill -vs");
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_vstab = 1;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageHflip(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "raspistill -hf");
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_hflip = 1;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraConfigureImageVflip(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    memset(command, 0, COMMAND_BUF_LEN);

    sprintf(command, "raspistill -vf");
    ret = SCPI_ExecuteCommand(command, 0);
    if (ret == SCPI_RES_OK) {
	global_conf.camera.image.f_vflip = 1;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CameraMemoryStore(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "w+");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    ret = camera_StoreConfiguration(fp);
    if (ret != SCPI_RES_OK)
    {
	fclose(fp);
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraMemoryDelete(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "r");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    ret = remove(absolute_path);
    if (ret != 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CameraMemoryLoad(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "r");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    ret = camera_LoadConfiguration(fp, absolute_path, len);
    if (ret != SCPI_RES_OK)
    {
	fclose(fp);
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureImage(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.f_image = 1;
    memset(global_conf.ocr.image_name, 0, (len + 1));
    memcpy(global_conf.ocr.image_name, param1, len);
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureImageQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tInput Image=%s\r\n", global_conf.ocr.image_name);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsVerbose(scpi_t * context) {

    global_conf.ocr.options.f_verbose = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsThreshold(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, THRESHOLD_MIN, THRESHOLD_MAX)
	global_conf.ocr.options.f_threshold = 1;
	global_conf.ocr.options.threshold = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsAbsoluteThreshold(scpi_t * context) {

    global_conf.ocr.options.f_absolute = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsIterThreshold(scpi_t * context){

    global_conf.ocr.options.f_iter = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsNumberPixels(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, PIXEL_MIN, PIXEL_MAX)
	global_conf.ocr.options.f_numberPixels = 1;
	global_conf.ocr.options.numberPixels = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsIgnorePixels(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, PIXEL_MIN, PIXEL_MAX)
	global_conf.ocr.options.f_ignorePixels = 1;
	global_conf.ocr.options.ignorePixels = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsNumberDigits(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, NUMBER_MIN, NUMBER_MAX)
	global_conf.ocr.options.f_numberDigits = 1;
        global_conf.ocr.options.numberDigits = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsOneRatio(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, RATIO_MIN, RATIO_MAX)
	global_conf.ocr.options.f_oneRatio = 1;
	global_conf.ocr.options.oneRatio = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsMinusRatio(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, RATIO_MIN, RATIO_MAX)
	global_conf.ocr.options.f_minusRatio = 1;
	global_conf.ocr.options.minusRatio = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsOutputImage(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.options.f_outputImage = 1;
    memset(global_conf.ocr.options.outputImage, 0, (len + 1));
    memcpy(global_conf.ocr.options.outputImage, param1, len);
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsOutputFormat(scpi_t * context) {
    int param;

    if (!SCPI_ParamChoice(context, trigger_format, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.options.f_outputFormat = 1;
    global_conf.ocr.options.outputFormat = param;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsProcessOnly(scpi_t * context) {

    global_conf.ocr.options.f_processOnly = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsDebugImage(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.options.f_debugImage = 1;
    memset(global_conf.ocr.options.debugImage, 0, (len + 1));
    memcpy(global_conf.ocr.options.debugImage, param1, len);
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsDebugOutput(scpi_t * context) {

    global_conf.ocr.options.f_debugOutput = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsForeground(scpi_t * context) {
    int param;

    if (!SCPI_ParamChoice(context, trigger_color, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.options.f_foreground = 1;
    global_conf.ocr.options.foreground = param;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsBackground(scpi_t * context) {
    int param;

    if (!SCPI_ParamChoice(context, trigger_color, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.options.f_background = 1;
    global_conf.ocr.options.background = param;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsPrintInfo(scpi_t * context) {

    global_conf.ocr.options.f_printInfo = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsAdjustGray(scpi_t * context) {

    global_conf.ocr.options.f_adjustGray = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsLuminance(scpi_t * context) {
    int param;

    if (!SCPI_ParamChoice(context, trigger_luminance, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.options.f_luminance = 1;
    global_conf.ocr.options.luminance = param;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsThresholdQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tThreshold=%d\r\n", global_conf.ocr.options.threshold);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsAbsoluteThresholdQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tAbsolute Threshold=%d\r\n", global_conf.ocr.options.f_absolute);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsIterThresholdQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tIterative Threshold=%d\r\n", global_conf.ocr.options.f_iter);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsNumberPixelsQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tNumber Pixels=%d\r\n", global_conf.ocr.options.numberPixels);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsIgnorePixelsQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tIgnore Pixels=%d\r\n", global_conf.ocr.options.ignorePixels);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsNumberDigitsQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tNumber Digits=%d\r\n", global_conf.ocr.options.numberDigits);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsOneRatioQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tone-ratio=%d\r\n", global_conf.ocr.options.oneRatio);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsMinusRatioQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tminus-ratio=%d\r\n", global_conf.ocr.options.minusRatio);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsOutputImageQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tOutput Image=%s\r\n", global_conf.ocr.options.outputImage);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsOutputFormatQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tOutput Format=%s\r\n", trigger_format[global_conf.ocr.options.outputFormat]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsDebugImageQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tDebug Image=%s\r\n", global_conf.ocr.options.debugImage);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsForegroundQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tForeground=%s\r\n", trigger_color[global_conf.ocr.options.foreground]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsBackgroundQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tBackground=%s\r\n", trigger_color[global_conf.ocr.options.background]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureOptionsLuminanceQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tLuminance=%s\r\n", trigger_luminance[global_conf.ocr.options.luminance]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsDilation(scpi_t * context) {

    global_conf.ocr.commands.f_dilation = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsErosion(scpi_t * context) {

    global_conf.ocr.commands.f_erosion = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsClosing(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, NUMBER_MIN, NUMBER_MAX)
	global_conf.ocr.commands.f_closing = 1;
	global_conf.ocr.commands.closing = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsOpening(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, NUMBER_MIN, NUMBER_MAX)
	global_conf.ocr.commands.f_opening = 1;
	global_conf.ocr.commands.opening = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsRemoveIsolated(scpi_t * context) {

    global_conf.ocr.commands.f_removeIsolated = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsMakeMono(scpi_t * context) {

    global_conf.ocr.commands.f_makeMono = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsGrayScale(scpi_t * context) {

    global_conf.ocr.commands.f_grayscale = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsInvert(scpi_t * context) {

    global_conf.ocr.commands.f_invert = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsStretch(scpi_t * context) {
    int param1, param2;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, STRETCH_MIN, STRETCH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    IF_RANGE (param2, STRETCH_MIN, STRETCH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    global_conf.ocr.commands.f_stretch = 1;
    global_conf.ocr.commands.stretchT1 = param1;
    global_conf.ocr.commands.stretchT2 = param2;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsDynamic(scpi_t * context) {
    int param1, param2;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    IF_RANGE (param2, HEIGHT_MIN, HEIGHT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    global_conf.ocr.commands.f_dynamic = 1;
    global_conf.ocr.commands.dynamicW = param1;
    global_conf.ocr.commands.dynamicH = param2;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsRgb(scpi_t * context) {

    global_conf.ocr.commands.f_rgb = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsRthreshold(scpi_t * context) {

    global_conf.ocr.commands.f_r = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsGthreshold(scpi_t * context) {

    global_conf.ocr.commands.f_g = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsBthreshold(scpi_t * context) {

    global_conf.ocr.commands.f_b = 1;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsWhiteBorder(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
	global_conf.ocr.commands.f_whiteBorder = 1;
	global_conf.ocr.commands.whiteBorder = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsShear(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
	global_conf.ocr.commands.f_shear = 1;
	global_conf.ocr.commands.shear = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsRotate(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, DEGREE_MIN, DEGREE_MAX)
	global_conf.ocr.commands.f_rotate = 1;
	global_conf.ocr.commands.rotate = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsMirror(scpi_t * context) {
    int param;

    if (!SCPI_ParamChoice(context, trigger_mirror, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.ocr.commands.f_mirror = 1;
    global_conf.ocr.commands.mirror = param;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsCrop(scpi_t * context) {
    int param1;
    int param2;
    int param3;
    int param4;


    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    }

    if (!SCPI_ParamInt(context, &param3, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    }

    if (!SCPI_ParamInt(context, &param4, false)) {
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    ENDIF_RANGE

    IF_RANGE (param2, HEIGHT_MIN, HEIGHT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    ENDIF_RANGE

    IF_RANGE (param3, WIDTH_MIN, WIDTH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    ENDIF_RANGE

    IF_RANGE (param4, HEIGHT_MIN, HEIGHT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return SCPI_RES_ERR;
    ENDIF_RANGE

    global_conf.ocr.commands.f_crop = 1;
    global_conf.ocr.commands.crop.x = param1;
    global_conf.ocr.commands.crop.y = param2;
    global_conf.ocr.commands.crop.w = param3;
    global_conf.ocr.commands.crop.h = param4;
    SCPI_ResultInt(context, EX_OK);

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsSetPixels(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
	global_conf.ocr.commands.f_setPixels = 1;
	global_conf.ocr.commands.setPixels = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsKeepPixels(scpi_t * context) {
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    IF_RANGE (param1, WIDTH_MIN, WIDTH_MAX)
	global_conf.ocr.commands.f_keepPixels = 1;
	global_conf.ocr.commands.keepPixels = param1;
	SCPI_ResultInt(context, EX_OK);
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
    ENDIF_RANGE

    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsClosingQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tclosing=%d\r\n", global_conf.ocr.commands.closing);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsOpeningQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\topening=%d\r\n", global_conf.ocr.commands.opening);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsStretchQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tstretch T1=%d T2=%d\r\n", global_conf.ocr.commands.stretchT1, \
	    global_conf.ocr.commands.stretchT2);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsDynamicQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tdynamic threshold W=%d H=%d\r\n", global_conf.ocr.commands.dynamicW, \
	    global_conf.ocr.commands.dynamicH);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsWhiteBorderQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\twhite border=%d\r\n", global_conf.ocr.commands.whiteBorder);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsShearQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tshear =%d\r\n", global_conf.ocr.commands.shear);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsRotateQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\trotate =%d\r\n", global_conf.ocr.commands.rotate);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsMirrorQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tmirror =%s\r\n", trigger_mirror[global_conf.ocr.commands.mirror]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsCropQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tCrop X=%d Y=%d W=%d H=%d\r\n", global_conf.ocr.commands.crop.x, \
	    global_conf.ocr.commands.crop.y, global_conf.ocr.commands.crop.w, \
	    global_conf.ocr.commands.crop.h);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsSetPixelsQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tset pixels filter=%d\r\n", global_conf.ocr.commands.setPixels);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrConfigureCommandsKeepPixelsQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tkeep pixels filter=%d\r\n", global_conf.ocr.commands.keepPixels);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrMemoryLoad(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "r");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    ret = ocr_LoadConfiguration(fp, absolute_path, len);
    if (ret != SCPI_RES_OK)
    {
	fclose(fp);
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrMemoryStore(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "w+");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    ret = ocr_StoreConfiguration(fp);
    if (ret != SCPI_RES_OK)
    {
	fclose(fp);
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_OcrMemoryDelete(scpi_t * context) {
    const char * param1;
    size_t len;
    FILE * fp = NULL;
    char absolute_path[FULL_BUF_LEN];
    scpi_result_t ret;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    len = get_absolutePath(param1, len, absolute_path);
    if (len <= 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
	return SCPI_RES_ERR;
    }

    fp = fopen(absolute_path, "r");
    if(fp == NULL){
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    fclose(fp);

    ret = remove(absolute_path);
    if (ret != 0)
    {
	SCPI_ResultInt(context, EX_SOFTWARE);
        return SCPI_RES_ERR;
    }

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_SystemIdnQ(scpi_t * context) {
    char ver[16];

    memset(result, 0, sizeof(COMMAND_BUF_LEN));

    SCPICAM_VERSION(ver);
    sprintf(result, "%s, %s, %04d, %s", SCPICAM_MANUFACTURER, \
	    SCPICAM_PRODUCT_NAME, SCPICAM_PRODUCT_NUMBER, ver);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_SystemDate(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;
    int param2;
    int param3;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    if (!SCPI_ParamInt(context, &param3, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, YEAR_MIN, YEAR_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param2, MONTH_MIN, MONTH_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param3, DAY_MIN, DAY_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    sprintf(command, "date +%Y%%m%%d -s %d%02d%02d", param1, param2, param3);
    ret = SCPI_ExecuteCommand(command, 1);
    if (ret == SCPI_RES_OK) {
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_SystemDateQ(scpi_t * context) {
    time_t cur_time;
    struct tm* timeinfo;

    memset(result, 0, sizeof(COMMAND_BUF_LEN));

    time(&cur_time);
    timeinfo = localtime(&cur_time);
    strftime(result, COMMAND_BUF_LEN, "%F", timeinfo);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_SystemTime(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;
    int param2;
    int param3;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    if (!SCPI_ParamInt(context, &param2, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    if (!SCPI_ParamInt(context, &param3, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, HOUR_MIN, HOUR_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param2, MINUTE_MIN, MINUTE_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    IF_RANGE (param3, SECOND_MIN, SECOND_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    sprintf(command, "date +%T -s %02d:%02d:%02d", param1, param2, param3);
    ret = SCPI_ExecuteCommand(command, 1);
    if (ret == SCPI_RES_OK) {
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_SystemTimeQ(scpi_t * context) {
    time_t cur_time;
    struct tm* timeinfo;

    memset(result, 0, sizeof(COMMAND_BUF_LEN));

    time(&cur_time);
    timeinfo = localtime(&cur_time);
    strftime(result, COMMAND_BUF_LEN, "%T", timeinfo);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateSerialReset(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    ret = UART_Control(UART_RESET, 0);
    if (ret == SCPI_RES_OK) {
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CommunicateSerialBaud(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    if (!SCPI_ParamChoice(context, trigger_baud, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    ret = UART_Control(UART_BAUD, atoi(trigger_baud[param]));
    if (ret == SCPI_RES_OK) {
	global_conf.comm.serial.f_baud = 1;
	global_conf.comm.serial.baud = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CommunicateSerialBaudQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    if (global_conf.comm.serial.f_baud)
	sprintf(result, "\tBaud rate=%s\r\n", trigger_baud[global_conf.comm.serial.baud]);
    else
	sprintf(result, "\tBaud rate=%s\r\n", trigger_baud[5]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateSerialCountrolRts(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    if (!SCPI_ParamChoice(context, trigger_rts, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    ret = UART_Control(UART_RTS, param);
    if (ret == SCPI_RES_OK) {
	global_conf.comm.serial.f_rts = 1;
	global_conf.comm.serial.rts = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CommunicateSerialCountrolRtsQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    if (global_conf.comm.serial.f_rts)
	sprintf(result, "\tRTS =%s\r\n", trigger_rts[global_conf.comm.serial.rts]);
    else
	sprintf(result, "\tRTS =%s\r\n", trigger_rts[RTS_ON]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateSerialEcho(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    if (!SCPI_ParamChoice(context, trigger_echo, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    param = param > ECHO_ON ? param - 2 : param;
    ret = UART_Control(UART_ECHO, param);
    if (ret == SCPI_RES_OK) {
	global_conf.comm.serial.f_echo = 1;
	global_conf.comm.serial.echo = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CommunicateSerialEchoQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    sprintf(result, "\tECHO =%s\r\n", trigger_echo[global_conf.comm.serial.echo]);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateSerialTimeout(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, TIMEOUT_MIN, TIMEOUT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    ret = UART_Control(UART_TIMEOUT, param1);
    if (ret == SCPI_RES_OK) {
	global_conf.comm.serial.f_tout = 1;
	global_conf.comm.serial.tout = param1;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateSerialTimeoutQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    if (global_conf.comm.serial.f_tout)
	sprintf(result, "\tTimeout =%d\r\n", global_conf.comm.serial.tout);
    else
	sprintf(result, "\tTimeout =%d\r\n", UART_DEFAULT_TIMEOUT);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateSerialReceivePace(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    if (!SCPI_ParamChoice(context, trigger_pace, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    ret = UART_Control(UART_RECEIVE_PACE, param);
    if (ret == SCPI_RES_OK) {
	global_conf.comm.serial.f_recv_xon = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CommunicateSerialReceivePaceQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    if (global_conf.comm.serial.f_recv_xon)
	sprintf(result, "\tReceive PACE = XON\r\n");
    else
	sprintf(result, "\tReceive PACE = NONE\r\n");
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateSerialTransmitPace(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param;

    if (!SCPI_ParamChoice(context, trigger_pace, &param, true)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    ret = UART_Control(UART_TRANSMIT_PACE, param);
    if (ret == SCPI_RES_OK) {
	global_conf.comm.serial.f_trans_xon = param;
	SCPI_ResultInt(context, EX_OK);
    } else {
	SCPI_ResultInt(context, EX_SOFTWARE);
    }

    return ret;
}

scpi_result_t SCPI_CommunicateSerialTransmitPaceQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    if (global_conf.comm.serial.f_trans_xon)
	sprintf(result, "\tTransmit PACE = XON\r\n");
    else
	sprintf(result, "\tTransmit PACE = NONE\r\n");
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateTcpControl(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;
    int param1;

    memset(command, 0, COMMAND_BUF_LEN);

    if (!SCPI_ParamInt(context, &param1, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return ret;
    }

    IF_RANGE (param1, PORT_MIN, PORT_MAX)
    ELSEIF_RANGE
	SCPI_ResultInt(context, EX_USAGE);
        return ret;
    ENDIF_RANGE

    global_conf.comm.tcp.f_port = 1;
    global_conf.comm.tcp.port = param1;

    global_conf.f_socket_restart = 1;
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateTcpControlQ(scpi_t * context) {

    memset(result, 0, RESULT_BUF_LEN);

    if (global_conf.comm.tcp.f_port)
	sprintf(result, "\tTCP port =%d\r\n", global_conf.comm.tcp.port);
    else
	sprintf(result, "\tTCP port =%d\r\n", TCP_SERVER_PORT);
    SCPI_ResultString(context, result);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateLanReset(scpi_t * context) {
    scpi_result_t ret = SCPI_RES_ERR;

    if (global_conf.comm.lan.f_ipaddr && global_conf.comm.lan.f_subnet) {
	memset(command, 0, COMMAND_BUF_LEN);
	memset(result, 0, COMMAND_BUF_LEN);
	sprintf(command, "ifconfig %s %s", NETIF_NAME, inet_ntoa(global_conf.comm.lan.ipaddr));
	sprintf(result, " netmask %s", inet_ntoa(global_conf.comm.lan.subnet));
	strcat(command, result);
	ret = SCPI_ExecuteCommand(command, 1);
	if (ret != SCPI_RES_OK)
	    return SCPI_RES_ERR;
    }

    if (global_conf.comm.lan.f_gateway) {
	memset(command, 0, COMMAND_BUF_LEN);
	sprintf(command, "route add default gw %s %s", inet_ntoa(global_conf.comm.lan.gateway), \
		NETIF_NAME);
	ret = SCPI_ExecuteCommand(command, 1);
	if (ret != SCPI_RES_OK)
	    return SCPI_RES_ERR;
    }

    if (global_conf.comm.lan.f_host) {
	memset(command, 0, COMMAND_BUF_LEN);
	sprintf(command, "hostname %s", global_conf.comm.lan.host);
	ret = SCPI_ExecuteCommand(command, 1);
	if (ret != SCPI_RES_OK)
	    return SCPI_RES_ERR;
    }

    if (global_conf.comm.lan.f_name1 || global_conf.comm.lan.f_name2) {
	FILE * fp = NULL;

	fp = fopen(_PATH_RESCONF, "w+");
	if(fp == NULL)
	    return SCPI_RES_ERR;

	if (global_conf.comm.lan.f_name1)
	    fprintf(fp, "nameserver %s\n", inet_ntoa(global_conf.comm.lan.name1));
	if (global_conf.comm.lan.f_name1)
	    fprintf(fp, "nameserver %s\n", inet_ntoa(global_conf.comm.lan.name2));

	fclose(fp);
    }

    global_conf.f_socket_restart = 1;
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateLanAddress(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (inet_aton(param1, &global_conf.comm.lan.ipaddr) == 0) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.comm.lan.f_ipaddr = 1;
    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateLanHostname(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.comm.lan.f_host = 1;
    memset(global_conf.comm.lan.host, 0, (len + 1));
    memcpy(global_conf.comm.lan.host, param1, len);

    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateLanSubnet(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (inet_aton(param1, &global_conf.comm.lan.subnet) == 0) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.comm.lan.f_subnet = 1;
    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateLanGateway(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (inet_aton(param1, &global_conf.comm.lan.gateway) == 0) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.comm.lan.f_gateway = 1;
    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateLanNameserver1(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (inet_aton(param1, &global_conf.comm.lan.name1) == 0) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.comm.lan.f_name1 = 1;
    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

scpi_result_t SCPI_CommunicateLanNameserver2(scpi_t * context) {
    const char * param1;
    size_t len;

    if (!SCPI_ParamString(context, &param1, &len, false)) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    if (inet_aton(param1, &global_conf.comm.lan.name2) == 0) {
	SCPI_ResultInt(context, EX_USAGE);
	return SCPI_RES_ERR;
    }

    global_conf.comm.lan.f_name2 = 1;
    SCPI_ResultInt(context, EX_OK);
    return SCPI_RES_OK;
}

