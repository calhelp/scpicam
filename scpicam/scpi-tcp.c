#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <arpa/inet.h>

#include "scpi/scpi.h"
#include "scpi-def.h"
#include "scpi-tcp.h"
#include "scpi-config.h"

int tcp_client_fd = -1;
pid_t tcp_cmd_pid = -1;

static int waitServer(int fd) {
    fd_set fds;
    struct timeval timeout;
    int rc;
    int max_fd;
    
    FD_ZERO(&fds);
    max_fd = fd;
    FD_SET(fd, &fds);
    
    timeout.tv_sec  = 5;
    timeout.tv_usec = 0;
    
    rc = select(max_fd + 1, &fds, NULL, NULL, &timeout);
    
    return rc;
}

int TCP_CreateServer(int port) {
    int fd;
    int rc;
    int on = 1;
    struct sockaddr_in servaddr;
        
    /* Configure TCP Server */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
    servaddr.sin_port=htons(port);
    
    /* Create socket */
    fd = socket(AF_INET,SOCK_STREAM, 0);
    if (fd < 0)
    {
        RPRINT("socket() failed");
        exit(-1);
    }    
    
    /* Set address reuse enable */
    rc = setsockopt(fd, SOL_SOCKET,  SO_REUSEADDR, (char *)&on, sizeof(on));
    if (rc < 0)
    {
        RPRINT("setsockopt() failed");
        close(fd);
        exit(-1);
    }
   
    /* Set non blocking */
    rc = ioctl(fd, FIONBIO, (char *)&on);
    if (rc < 0)
    {
        RPRINT("ioctl() failed");
        close(fd);
        exit(-1);
    }    
    
    /* Bind to socket */
    rc = bind(fd, (struct sockaddr *)&servaddr, sizeof(servaddr));
    if (rc < 0)
    {
        RPRINT("bind() failed");
        close(fd);
        exit(-1);
    }
    
    /* Listen on socket */
    listen(fd, 1);
    if (rc < 0)
    {
        RPRINT("listen() failed");
        close(fd);
        exit(-1);
    }
    
    return fd;
}

int TCP_Receive(int fd) {
    char rx_buffer[TCP_RX_BUF_LEN];
    int rx_length;
    struct sockaddr_in cliaddr;
    socklen_t len;
    struct scpicam * global_conf = NULL;

    global_conf = GetConfiguration();
    if (global_conf == NULL)
	return -1;

    if (fd < 0)
	return -1;

    global_conf->f_socket_restart = 0;

    len = sizeof(cliaddr);
    tcp_client_fd = accept(fd, (struct sockaddr *)&cliaddr, &len);

    if (tcp_client_fd < 0) return -1;

    RPRINT("Connection established %s\r\n", inet_ntoa(cliaddr.sin_addr));

    while(1) {
	rx_length = waitServer(tcp_client_fd);
	if (rx_length < 0) { // failed
	    DPRINT("  recv() failed %d(ret %d)\n", __LINE__, errno);
	} else if (rx_length == 0) { // timeout
	    SCPI_Input(&scpi_context, NULL, 0);
	} else {
	    rx_length = recv(tcp_client_fd, rx_buffer, sizeof(rx_buffer), 0);
	    if (rx_length < 0) {
		if (errno != EWOULDBLOCK) {
		    RPRINT("  recv() failed %d\n", __LINE__);
		    break;
		}
	    } else if (rx_length == 0) {                
		RPRINT("Connection closed\r\n");
		break;
	    } else {
		rx_buffer[rx_length] = '\0';
		DPRINT("TCP RX %s\n", rx_buffer);

		if (global_conf->f_running) {
		    SCPI_Input(&scpi_context, rx_buffer, rx_length);
		    if (!global_conf->f_running) {
			global_conf->f_operating = 0;
			if (tcp_cmd_pid > 0) {
			    kill(tcp_cmd_pid, SIGTERM);
			}
		    } else {
			global_conf->f_operating = 0;
			global_conf->f_running = 0;
		    }
		} else {
		    SCPI_Input(&scpi_context, rx_buffer, rx_length);
		    if (global_conf->f_running) {
			global_conf->f_operating = 1;
		    } else {
			global_conf->f_operating = 0;
		    }
		}

		if (global_conf->f_operating) {
		    if (tcp_cmd_pid < 0)
			tcp_cmd_pid = fork();
		    if (tcp_cmd_pid == 0) {
			SCPI_Input(&scpi_context, rx_buffer, rx_length);
			DPRINT("tcp execution\n");
			exit(0);
		    }

		    global_conf->f_operating = 0;
		}

		LoadConfiguration();

		if (global_conf->f_socket_restart)
		    break;
	    }
	}
    }

    close(tcp_client_fd);

    return 0;
}

int TCP_Transmit(int fd, const char * data, size_t len) {
    int count = -1;

    if (fd < -1)
	return -1;

    DPRINT("TCP TX %s %d\n", data, len);

    count = send(tcp_client_fd, data, len, 0);
    if (count < 0)
    {
	DPRINT("TCP TX error\n");
	return -1;
    }

    return count;
}
