#ifndef __SCPI_TCP_H_
#define __SCPI_TCP_H_

#define TCP_SERVER_PORT	5025
#define TCP_ACCEPT_INTERVAL 2000 // useconds
#define TCP_RX_BUF_LEN  256

int TCP_CreateServer(int port);
int TCP_Receive(int fd);
int TCP_Transmit(int fd, const char * data, size_t len);

#endif // __SCPI_TCP_H_
