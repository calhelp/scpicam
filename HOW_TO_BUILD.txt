SCPICAM is depends on userland of raspberrypi.
For userland, please reference site https://github.com/raspberrypi/userland

1. For cross-compiling, download toolchain
git clone https://github.com/raspberrypi/tools

2. Set PATH environment about toolchain
export $PATH=$(HOME)/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin:$PATH

3. Download userland
git clone https://github.com/raspberrypi/userland

4. Download scpicam
git clone https://USERNAME@bitbucket.org/calhelp/scpicam.git
* For downloading scpicam from bitbucket by using git, please reference site following by
- https://confluence.atlassian.com/display/BITBUCKET/Set+up+Git+and+Mercurial
- https://confluence.atlassian.com/display/BITBUCKET/Clone+Your+Git+Repo+and+Add+Source+Files

5. Add scpicam in userland.
mv $(HOME)/scpicam/* $(HOME)/userland/host_applications/linux/apps/

6. Modify CMakeLists.txt
please add followed by commands
- add_subdirectory(apps/scpi-parser)
- add_subdirectory(apps/scpicam)

7. Build userland
For building successfully, you 
cd $(HOME)/userland/
./buildme

8. Verify binaries and libraries
Perhaps, you can verify its in directory $(HOME)/userland/build